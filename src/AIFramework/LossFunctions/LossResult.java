package AIFramework.LossFunctions;

import java.io.Serializable;

import AIFramework.Computation.Matrix;

public class LossResult implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4382135568542266575L;
	public double loss;
	public Matrix<Double> matrix;
	
	public LossResult duplicate(){
		LossResult ret= new LossResult();
		ret.loss = this.loss;
		ret.matrix = this.matrix.duplicate();
		return ret;
	}

}

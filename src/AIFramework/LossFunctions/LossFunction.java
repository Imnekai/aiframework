package AIFramework.LossFunctions;

import java.io.Serializable;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;

public abstract class LossFunction implements Serializable{
	
	protected double regularization_loss=0;
	
	public void setRegularizationLoss(double reg){
		this.regularization_loss = reg;
	}
	
	public double getRegularizationLoss(){
		return this.regularization_loss;
	}
	
	public abstract LossResult compute(Matrix<Double> input,Matrix<Double> labels);
	public abstract double loss(Matrix<Double> input,Matrix<Double> labels);
	
	public Matrix<Double> aproxGradient(Matrix<Double> input, Matrix<Double> labels){
		Matrix<Double> gradient= Matrix.newInstance(input.rowNr(),input.colNr());
		double t = Config.EPSILON;
		for(int i=0;i<input.size();++i){
			input.put(i,input.get(i)+t);
			double f1 = loss(input,labels);
			input.put(i, input.get(i)-2*t);
			double f2 = loss(input,labels);
			gradient.put(i,(f1-f2)/(2*t));
			input.put(i, input.get(i)+t);
		}
		return gradient;
	}
	
	public abstract LossFunction duplicate();

}

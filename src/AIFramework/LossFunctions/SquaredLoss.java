package AIFramework.LossFunctions;

import AIFramework.Computation.Matrix;

public class SquaredLoss extends LossFunction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2243492045117830813L;

	@Override
	public LossResult compute(Matrix<Double> input,Matrix<Double> labels) {
		if(!input.match(labels))throw new IllegalArgumentException("Input matrix must have same dimensions as the labels matrix.");
		LossResult result = new LossResult();
		result.matrix = input.sub(labels);
		result.loss = 0.5d*result.matrix.emul(result.matrix).getSum()/input.rowNr()+this.regularization_loss;
		result.matrix.muli(1d/input.rowNr());
		return result;
	}

	@Override
	public double loss(Matrix<Double> input, Matrix<Double> labels) {
		Matrix<Double> aux = input.sub(labels);
		return 0.5d*aux.emul(aux).getSum()/input.rowNr()+this.regularization_loss;
	}
	
	public SquaredLoss duplicate(){
		SquaredLoss ret = new SquaredLoss();
		ret.regularization_loss = this.regularization_loss;
		return ret;
	}

}

package AIFramework.LossFunctions;

import AIFramework.Computation.ExpFunction;
import AIFramework.Computation.LogFunction;
import AIFramework.Computation.Matrix;

public class CrossEntropyLoss extends LossFunction{
	
	private ExpFunction exp = new ExpFunction();
	private LogFunction log = new LogFunction();

	@Override
	public LossResult compute(Matrix<Double> input, Matrix<Double> labels) {
		LossResult ret = new LossResult();
		Matrix<Double> input_aux = exp.call(input.subColumns(input.getRowMaxs()));
		Matrix<Double> normalized = input_aux.diviColumns(input_aux.getRowSum());
		ret.loss = -log.call(normalized.emul(labels)).getSum()/input.rowNr()+this.regularization_loss;
		ret.matrix = normalized.subi(labels).muli(1d/input.rowNr());
		return ret;
	}

	@Override
	public double loss(Matrix<Double> input, Matrix<Double> labels) {
		Matrix<Double> input_aux = exp.call(input.subColumns(input.getRowMaxs()));
		Matrix<Double> normalized = input_aux.diviColumns(input_aux.getRowSum());
		return -log.call(normalized.emul(labels)).getSum()/input.rowNr()+this.regularization_loss;
	}

	@Override
	public LossFunction duplicate() {
		CrossEntropyLoss ret = new CrossEntropyLoss();
		ret.regularization_loss=this.regularization_loss;
		return ret;
	}

}

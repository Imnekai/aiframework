package AIFramework.SupervisedModels;

import java.util.ArrayList;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Utility;
import AIFramework.Data.DataPoint;

public class LinearClassificationModel extends LinearRegressionModel{

	private static final long serialVersionUID = -3798915037845868361L;

	
	@Override
	public Matrix<Double> predictInternal(Matrix<Double> data_points) {
		Matrix<Double> scores = super.predictInternal(data_points);
		return scores.getRowMaxsInd();
	}
}

package AIFramework.SupervisedModels;

import java.util.ArrayList;
import java.util.Arrays;

import AIFramework.Computation.Matrix;
import AIFramework.Core.LearningModel;
import AIFramework.Core.Utility;
import AIFramework.Data.DataSet;
import AIFramework.LossFunctions.LossFunction;
import AIFramework.LossFunctions.LossResult;
import AIFramework.NNlayers.FClayer;
import AIFramework.NNlayers.NNlayer;

public class NeuralNetwork extends SupervisedModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8237774626305632903L;
	
	protected ArrayList<NNlayer> layers=null;
	protected LossFunction loss_function;
	protected int input_dimension;
	protected double learning_rate=1.0E-3;
	protected double lr_decay = 1d;
	protected int batch_size=50,epochs=30;
	protected boolean verbose = false;
	protected double momentum = 0;
	
	protected Matrix<Double> validation=null,validation_labels=null;
	
	public NeuralNetwork(LossFunction loss_function,int input_dimension){
		this.loss_function = loss_function;
		this.input_dimension = input_dimension;
		this.layers = new ArrayList<NNlayer>();
	}
	
	public void setVerbose(boolean verbose){
		this.verbose = verbose;
	}
	
	public int getInputDimension(){
		return this.input_dimension;
	}
	
	public ArrayList<NNlayer> getLayers(){
		return this.layers;
	}
	
	public void setMomentum(double momentum){
		this.momentum = momentum;
	}
	
	public double getMomentum(){
		return this.momentum;
	}
	
	public double getLearningRate(){
		return this.learning_rate;
	}
	
	public void setLearningRate(double lr){
		this.learning_rate = lr;
	}
	
	public void setLearningRateDecay(double lr_decay){
		this.lr_decay = lr_decay;
	}
	
	public double getLearningRateDecay(){
		return this.lr_decay;
	}
	
	public int getBatchSize(){
		return this.batch_size;
	}
	
	public void setBatchSize(int size){
		this.batch_size = size;
	}
	
	public int getEpochs(){
		return this.epochs;
	}
	
	public void setEpochs(int epochs){
		this.epochs = epochs;
	}
	
	public void setValidationSet(DataSet validation_set){
		this.validation = this.preprocessor.processi(validation_set.getLabeledFeatureMatrix());
		this.validation_labels = validation_set.getLabelMatrix();
		checkInput(this.validation);
	}
	
	public void addLayer(NNlayer layer){
		this.layers.add(layer);
	}
	
	
	protected void checkInput(Matrix<Double> input){
		if(this.layers==null||this.layers.size()==0)
			throw new IllegalStateException("Neural network does not have layers.");
		if(input.colNr()!=this.input_dimension)
			throw new IllegalArgumentException(String.format("Data does not match the expected dimension (%d expected, %d received)",
					this.input_dimension,input.colNr()));
	}
	
	public LossResult batchUpdate(Matrix<Double> design_matrix,Matrix<Double> labels){
		LossResult ret;
		Matrix<Double> input = design_matrix;
		double reg_loss = 0d;
		for(NNlayer layer:layers){
			input = layer.forward(input, true);
			reg_loss+=layer.getRegLoss();
		}
		loss_function.setRegularizationLoss(reg_loss);
		ret = this.loss_function.compute(input, labels);
		for(int i=layers.size()-1;i>=0;i--){
			if(layers.get(i).getClass()==FClayer.class){
				((FClayer)layers.get(i)).setLearningRate(this.learning_rate);
				((FClayer)layers.get(i)).setMomentum(this.momentum);
				((FClayer)layers.get(i)).setRegularization(this.regularization);
			}
			ret.matrix = layers.get(i).backward(ret.matrix, true);
		}
		return ret;
	}
	
	public LossResult loss(Matrix<Double> design_matrix,Matrix<Double> labels){
		LossResult ret = new LossResult();
		Matrix<Double> input = design_matrix;
		double reg_loss = 0d;
		for(NNlayer layer:layers){
			input = layer.forward(input, false);
			reg_loss+=layer.getRegLoss();
		}
		loss_function.setRegularizationLoss(reg_loss);
		ret.matrix = input;
		ret.loss = loss_function.loss(input, labels);
		return ret;
	}
	
	protected Matrix<Double> forwardInternal(Matrix<Double> design_matrix){
		checkInput(design_matrix);
		Matrix<Double> input = design_matrix;
		for(NNlayer layer:layers){
			input = layer.forward(input, false);
		}
		return input;
	}
	

	@Override
	protected void trainInternal(Matrix<Double> training_set, Matrix<Double> labels) {		
		int nr_examples = training_set.rowNr();
		int batch_size = Math.min(this.batch_size, nr_examples);
		int val_batch_size,val_size=0;
		if(this.validation!=null)
			val_size = validation.rowNr();
			val_batch_size = Math.min(this.batch_size, val_size);
		
		int iterations_per_epoch = nr_examples/batch_size;
		NeuralNetwork best_nn = null;
		double best_loss=0;
		LossResult res=new LossResult();
		for(int epoch=0;epoch<this.epochs;++epoch){
			int[] inds = Utility.randInts(0, nr_examples-1, nr_examples);
			res.loss = 0;
			for(int it=0;it<iterations_per_epoch;++it){
				int[] range = Arrays.copyOfRange(inds, it*batch_size, (it+1)*batch_size);
				Matrix<Double> batch = training_set.getRows(range);
				Matrix<Double> batch_labels = labels.getRows(range);
				res.loss += this.batchUpdate(batch, batch_labels).loss/iterations_per_epoch;
			}
			
			if(best_nn==null||best_loss>res.loss){
				best_nn=this.duplicate();
				best_loss = res.loss;
			}
			
			if(verbose){
				if(this.lr_decay==1)
					System.out.println(String.format("Epoch %d loss: %f",epoch,res.loss));
				else
					System.out.println(String.format("Epoch %d loss: %f learning_rate: %f",epoch,res.loss,this.learning_rate));
				if(this.validation!=null){
					inds = Utility.randInts(0, val_size-1, val_batch_size);
					Matrix<Double> batch = validation.getRows(inds);
					Matrix<Double> batch_labels = validation_labels.getRows(inds);
					double validation_loss = this.loss(batch, batch_labels).loss;
					System.out.println("Validation loss: "+validation_loss);
				}
			}
			this.learning_rate*=this.lr_decay;
		}
		if(best_nn!=null&&best_loss<res.loss)
			this.copy(best_nn);
		
	}

	@Override
	protected Matrix<Double> predictInternal(Matrix<Double> design_matrix) {
		checkInput(design_matrix);
		return this.forwardInternal(design_matrix);
	}
	
	public void copy(NeuralNetwork other){
		this.loss_function=other.loss_function.duplicate();
		this.input_dimension = other.input_dimension;
		this.basis_function = other.basis_function.duplicate();
		if(other.preprocessor!=null)
			this.preprocessor = other.preprocessor.duplicate();
		this.regularization=other.regularization;
		this.batch_size=other.batch_size;
		this.epochs = other.epochs;
		if(other.validation!=null)
			this.validation=other.validation.duplicate();
		if(other.validation_labels!=null)
			this.validation_labels=other.validation_labels.duplicate();
		this.layers = new ArrayList<NNlayer>();
		for(NNlayer layer:other.layers)
			this.layers.add(layer.duplicate());
	}
	
	public NeuralNetwork duplicate(){
		NeuralNetwork ret= new NeuralNetwork(this.loss_function.duplicate(),this.input_dimension);
		ret.copy(this);
		return ret;
	}

}

package AIFramework.SupervisedModels;

import java.util.ArrayList;

import AIFramework.Computation.Matrix;
import AIFramework.LossFunctions.LossFunction;
import AIFramework.LossFunctions.LossResult;
import AIFramework.NNlayers.FClayer;
import AIFramework.NNlayers.NNlayer;
import AIFramework.NNlayers.ReluLayer;

public class FCNN extends NeuralNetwork{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4005383159445531319L;
	
	protected int output_size;
	
	public FCNN(LossFunction loss_function,int input_dimension){
		super(loss_function,input_dimension);
	}
	
	public void set(Integer...layers_sizes){
		if(layers_sizes.length==0)throw new IllegalArgumentException();
		this.layers = new ArrayList<NNlayer>();
		int last_size = this.input_dimension;
		for(int i=0;i<layers_sizes.length;++i){
			FClayer fc_layer = null;
			if(i<layers_sizes.length-1)
				fc_layer = new FClayer(last_size,layers_sizes[i],true);
			else
				fc_layer = new FClayer(last_size,layers_sizes[i],false);
			fc_layer.setRegularization(this.regularization);
			fc_layer.setLearningRate(this.learning_rate);
			fc_layer.setMomentum(this.momentum);
			layers.add(fc_layer);
			last_size = layers_sizes[i];
			if(i!=layers_sizes.length-1)
				layers.add(new ReluLayer());
		}
		output_size = last_size;
	}
	
	
	@Override
	public void addLayer(NNlayer layer){
		if(layer.getClass()==FClayer.class){
			output_size=((FClayer)layer).getSize();
			((FClayer)layer).setLearningRate(this.learning_rate);
			((FClayer)layer).setMomentum(this.momentum);
			((FClayer)layer).setRegularization(this.regularization);
		}
		this.layers.add(layer);
	}
	
	protected void checkInput(Matrix<Double> input,Matrix<Double> labels){
		checkInput(input);
		if(labels.colNr()!=(output_size))
			throw new IllegalArgumentException("Labels does not match the expected dimension");
	}

	@Override
	public LossResult batchUpdate(Matrix<Double> design_matrix,Matrix<Double> labels){
		checkInput(design_matrix,labels);
		return super.batchUpdate(design_matrix, labels);
	}
	
	@Override
	public LossResult loss(Matrix<Double> design_matrix,Matrix<Double> labels){
		checkInput(design_matrix,labels);
		return super.loss(design_matrix, labels);
	}
	
	@Override
	protected Matrix<Double> forwardInternal(Matrix<Double> design_matrix){
		checkInput(design_matrix);
		return super.forwardInternal(design_matrix);
	}
	

	@Override
	protected void trainInternal(Matrix<Double> training_set, Matrix<Double> labels) {
		checkInput(training_set,labels);
		if(this.validation!=null)
			checkInput(this.validation,this.validation_labels);
		
		super.trainInternal(training_set, labels);
	}

	@Override
	protected Matrix<Double> predictInternal(Matrix<Double> design_matrix) {
		checkInput(design_matrix);
		return this.forwardInternal(design_matrix);
	}
	
	
	public FCNN duplicate(){
		FCNN ret= new FCNN(this.loss_function.duplicate(),this.input_dimension);
		ret.copy(this);
		return ret;
	}

}

package AIFramework.SupervisedModels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Core.LearningModel;
import AIFramework.Core.Utility;
import AIFramework.Data.BasisFunction;
import AIFramework.Data.BasisKernelFunction;
import AIFramework.Data.DataPoint;
import AIFramework.Data.DataSet;
import AIFramework.Data.ExtendedIdentityBasisFunction;
import AIFramework.Data.IdentityBasisFunction;
import AIFramework.Data.KernelFunction;
import AIFramework.Data.LabeledDataPoint;

public abstract class SupervisedModel extends LearningModel{
	
	protected Double regularization=0.5d;

	
	@Override
	public void train(DataSet train_set){
		this.train(train_set.getLabeledFeatureMatrix(), train_set.getLabelMatrix());
	}
	
	public void train(Matrix<Double> data_points, Matrix<Double> labels){
		this.preprocessor.processi(data_points);
		if(kernel!=null)
			this.basis_function = new BasisKernelFunction(data_points,kernel);
		if(this.basis_function==null)
			this.basis_function=new IdentityBasisFunction(data_points.colNr());
		Matrix<Double> design_matrix = this.basis_function.call(data_points); 
		this.trainInternal(design_matrix, labels);
	}
	
	
	
	/**
	 * Receives a preprocessed matrix and the corresponding labels
	 * @param design_matrix
	 * @param labels
	 */
	protected abstract void trainInternal(Matrix<Double> design_matrix,Matrix<Double> labels);
	
	/**
	 * Receives a preprocessed matrix
	 * @param design_matrix
	 * @return
	 */
	
	public void setRegularization(Double reg){
		this.regularization=reg;
	}
	
	public Double getRegularization(){
		return this.regularization;
	}

	public double test(DataSet test_set) {
		double ret=0d;
		Matrix<Double> pred = this.predict(test_set);
		Matrix<Double> labels = test_set.getLabelMatrix();
		Matrix<Double> inf_norm = pred.sub(labels).getRowsAbs();
		for(int i=0;i<inf_norm.size();++i)
			if(inf_norm.get(i)<Config.EPSILON)
				ret+=1d;
		return ret/labels.rowNr();
	}
	
	
	public double test1ofK(DataSet test_set){
		double ret = 0d;
		Matrix<Double> pred = this.predict(test_set);
		Matrix<Double> labels = test_set.getLabelMatrix();
		for(int i=0;i<pred.rowNr();++i){
			double max=pred.get(i, 0);
			int max_pos=0;
			for(int j=1;j<pred.colNr();++j)
				if(max<pred.get(i, j)){
					max=pred.get(i, j);
					max_pos = j;
				}
			if(labels.get(i, max_pos)==1)
				ret+=1;
		}
		return ret/pred.rowNr();
	}
	
	public Matrix<Double> confusionMatrix1ofK(DataSet test_set){
		Matrix<Double> labels = test_set.getLabelMatrix();
		Matrix<Double> ret = Matrix.newInstance(labels.colNr(),labels.colNr());
		Matrix<Double> pred = this.predict(test_set);
		for(int i=0;i<pred.rowNr();++i){
			double max=pred.get(i, 0);
			int max_pos=0;
			int one_pos=0;
			for(int j=1;j<pred.colNr();++j){
				if(labels.get(i,j)==1)
					one_pos=j;
				if(max<pred.get(i, j)){
					max=pred.get(i, j);
					max_pos = j;
				}
			}
			ret.put(one_pos, max_pos, ret.get(one_pos, max_pos)+1);
		}
		return ret;
	}
	
	

}

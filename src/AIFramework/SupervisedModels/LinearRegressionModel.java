package AIFramework.SupervisedModels;

import java.util.ArrayList;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Core.LearningModel;
import AIFramework.Data.BasisFunction;
import AIFramework.Data.BasisKernelFunction;
import AIFramework.Data.DataPoint;
import AIFramework.Data.DataSet;
import AIFramework.Data.ExtendedIdentityBasisFunction;
import AIFramework.Data.KernelFunction;

public class LinearRegressionModel extends SupervisedModel{
	

	private static final long serialVersionUID = 8108511943063127982L;
	
	protected Matrix<Double> weights;
	

	@Override
	public void trainInternal(Matrix<Double> design_matrix,Matrix<Double> labels) {
		Matrix<Double> design_matrix_t = design_matrix.transpose();
		this.weights = (Matrix.createIdentity(this.basis_function.getFinalDimension()).muli(this.regularization).addi(design_matrix_t.mul(design_matrix))).inverse().muli(design_matrix_t).mul(labels);
	}
	
	
	@Override
	public Matrix<Double> predictInternal(Matrix<Double> design_matrix) {
		return design_matrix.mul(this.weights);
	}
	
	
	public Matrix<Double> getWeights(){
		return this.weights;
	}


	@Override
	public LinearRegressionModel duplicate() {
		LinearRegressionModel ret = new LinearRegressionModel();
		ret.weights = this.weights.duplicate();
		ret.basis_function = this.basis_function.duplicate();
		ret.preprocessor = this.preprocessor.duplicate();
		ret.regularization=this.regularization;
		ret.kernel = this.kernel.duplicate();
		return ret;
	}

	

}

package AIFramework.Core;

import java.util.ArrayList;
import java.util.Random;

public class Utility {
	
	public static <T extends Comparable<T>> int indexOfMax(ArrayList<T> array_list){
		if(array_list==null||array_list.size()==0)throw new NullPointerException();
		T max = array_list.get(0);
		int ret=0;
		for(int i=1;i<array_list.size();++i)
			if(array_list.get(i).compareTo(max)>0){
				max = array_list.get(i);
				ret = i;
			}
		return ret;
	}
	
	public static int indexOfMax(double[] array){
		if(array==null)
			throw new NullPointerException("Argument is null");
		int ind = 0;
		double max = array[0];
		for(int i=1;i<array.length;++i)
			if(array[i]>max){
				max = array[i];
				ind=i;
			}
		return ind;
	}
	
	public static Random r=new Random();
	
	public static double rand(double a,double b){
		return r.nextDouble()*(b-a)+a;
	}
	
	/**
	 * Generates (nr) distinct integers in range [min,max]
	 * @param min
	 * @param max
	 * @param nr
	 * @return
	 */
	public static int[] randInts(int min,int max,int nr){
		if(max<min)throw new IllegalArgumentException();
		int range = max-min+1;
		if(nr>range)throw new IllegalArgumentException("Requested number of elements is bigger then the range");
		int[] gen = new int[range];
		for(int i=min;i<=max;++i)
			gen[i-min]=i;
		int[] ret = new int[nr];
		for(int i=0;i<nr;++i){
			int ind = r.nextInt(range);
			ret[i] = gen[ind];
			gen[ind] = gen[range-1];
			range--;
		}
		return ret;
	}
	
	public static int[] firstK(int k){
		int[] ret = new int[k];
		for(int i=0;i<k;++i)
			ret[i]=i;
		return ret;
	}
	
	public static boolean areEqual(double a,double b){
		return Math.abs(a-b)<Config.EPSILON;
	}
	
	public static int round(double x){
		return (int)Math.round(x);
	}
	
	public static int[] toArray(ArrayList<Integer> list){
		if(list==null||list.size()==0)
			return null;
		int ret[] = new int[list.size()];
		for(int i=0;i<list.size();++i)
			ret[i]=list.get(i);
		return ret;
	}

}

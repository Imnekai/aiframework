package AIFramework.Core;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Computation.Matrix;

public class Config {
	
	public static final double EPSILON = (double) 1.0E-7;
	public static Class<? extends Matrix<Double>> MatrixClass = JblasMatrix.class;
	
	public static void setMatrixClass(Class<? extends Matrix<Double>> MatrixClass){
		Config.MatrixClass = MatrixClass;
	}

}

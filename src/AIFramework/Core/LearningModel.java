package AIFramework.Core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import AIFramework.Computation.Matrix;
import AIFramework.Data.BasisFunction;
import AIFramework.Data.BasisKernelFunction;
import AIFramework.Data.DataPoint;
import AIFramework.Data.DataSet;
import AIFramework.Data.ExtendedIdentityBasisFunction;
import AIFramework.Data.IdentityBasisFunction;
import AIFramework.Data.KernelFunction;
import AIFramework.Data.Preprocessor;

public abstract class LearningModel implements Serializable{
	
	protected BasisFunction basis_function=null;
	protected KernelFunction kernel=null;
	
	public void setBasisFunction(BasisFunction basis_function){
		this.basis_function=basis_function;
		this.kernel = null;
	}
	
	public void setKernelFunction(KernelFunction kernel){
		this.kernel = kernel;
	}
	
	public abstract void train(DataSet train_set);
	
	public Matrix<Double> confusionMatrix(DataSet test_set,int num_classes){
		Matrix<Double> labels = test_set.getLabelMatrix();
		Matrix<Double> ret = Matrix.newInstance(num_classes,num_classes);
		Matrix<Double> pred = this.predict(test_set);
		for(int i=0;i<pred.rowNr();++i){
			ret.put(Utility.round(labels.get(i)), Utility.round(pred.get(i)), 
					ret.get(Utility.round(labels.get(i)), Utility.round(pred.get(i)))+1);
		}
		return ret;
	}
	
	
	public Matrix<Double> predict(DataPoint data_point){
		return predict(data_point.getFeatureVector());
	}
	
	public Matrix<Double> predict(DataSet train_set){
		return predict(train_set.getFeatureMatrix());
	}
	
	public Matrix<Double> predict(Matrix<Double> data_points){
		Matrix<Double> feature_matrix = this.preprocessor.process(data_points);
		if(this.basis_function==null)
			this.basis_function = new ExtendedIdentityBasisFunction(feature_matrix.colNr()); 
		feature_matrix = this.basis_function.call(feature_matrix);
		return this.predictInternal(feature_matrix);
	}
	
	protected abstract Matrix<Double> predictInternal(Matrix<Double> design_matrix);
	
	protected Preprocessor preprocessor=new Preprocessor();
	
	public void setCenterData(boolean val){
		this.preprocessor.setCenter(val);
	}
	
	public void setNormalizeData(boolean val){
		this.preprocessor.setNormalize(val);
	}
	
	public void setPCA(int nr_components){
		this.preprocessor.setPCA(nr_components);
	}
	
	public void setPreprocessor(DataSet train_data){
		this.preprocessor.setParams(train_data.getFeatureMatrix());
	}
	
	public boolean getCenterData(){
		return preprocessor.getCenter();
	}
	
	public boolean getNormalizeData(){
		return preprocessor.getNormalize();
	}
	
	public Preprocessor getPreprocesoor(){
		return this.preprocessor;
	}
	
	public abstract LearningModel duplicate();
	
	public void writeToFile(String file_path){
		  try
		  {
		     FileOutputStream fileOut = new FileOutputStream(file_path);
		     ObjectOutputStream out = new ObjectOutputStream(fileOut);
		     out.writeObject(this);
		     out.close();
		     fileOut.close();
		  }catch(IOException i)
		  {
		      i.printStackTrace();
		  }
	}
	
	public static LearningModel readFromFile(String file_path){
		LearningModel ret=null;
		  try
	      {
	         FileInputStream fileIn = new FileInputStream(file_path);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         ret = (LearningModel) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	      }catch(ClassNotFoundException c)
	      {
	         c.printStackTrace();
	      }
		  return ret;
	}
	

}

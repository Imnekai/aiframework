package AIFramework.Data;

import java.io.Serializable;

import AIFramework.Computation.Matrix;

public abstract class KernelFunction implements Serializable{
	
	protected abstract Double function(Matrix<Double> v1,Matrix<Double> v2);
	
	public Double call(Matrix<Double> vector1,Matrix<Double> vector2){
		if(vector1==null||vector2==null)throw new NullPointerException();
		if(!vector1.isVector()||!vector2.isVector())throw new IllegalArgumentException("Arguments are not vectors.");
		if(vector1.size()!=vector2.size())throw new IllegalArgumentException("Data Points must have same dimensions");
		return function(vector1,vector2);
	}
	
	public Double call(DataPoint dp1,DataPoint dp2){
		if(dp1==null||dp2==null)throw new NullPointerException();
		if(dp1.getDimension()!=dp2.getDimension())throw new IllegalArgumentException("Data Points must have same dimensions");
		return call(dp1.getFeatureVector(),dp2.getFeatureVector());
	}
	
	public abstract KernelFunction duplicate();

}

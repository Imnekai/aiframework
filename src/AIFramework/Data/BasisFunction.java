package AIFramework.Data;

import java.io.Serializable;

import AIFramework.Computation.Matrix;

public abstract class BasisFunction implements Serializable{

	protected int start_dimension,final_dimension;
	
	public BasisFunction(int start_dimension,int final_dimension){
		if(start_dimension<=0||final_dimension<=0)throw new IllegalArgumentException("Dimensions must be strictly positive");
		this.start_dimension=start_dimension;
		this.final_dimension=final_dimension;
	}
	
	protected abstract Matrix<Double> function(Matrix<Double> matrix);
		
	/**
	 * Call the basis function on the data point
	 * @param data_point
	 * @return Line vector of the projected features
	 */
	public Matrix<Double> call(DataPoint data_point){
		if(data_point==null)throw new NullPointerException();
		if(data_point.getDimension()!=start_dimension)
			throw new IllegalArgumentException("DataPoint dimention does not match the basis function start dimension");
		return function(data_point.getFeatureVector());
	}
	
	public Matrix<Double> call(Matrix<Double> matrix){
		if(matrix==null)throw new NullPointerException();
		if(matrix.colNr()!=start_dimension)
			throw new IllegalArgumentException("Matrix dimention does not match the basis function start dimension");
		return function(matrix);
	}
	
	public int getFinalDimension(){
		return this.final_dimension;
	}
	
	public int getStartDimension(){
		return this.start_dimension;
	}
	
	public abstract BasisFunction duplicate();
}

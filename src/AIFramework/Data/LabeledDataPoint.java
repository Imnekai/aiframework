package AIFramework.Data;

import java.util.Arrays;

public class LabeledDataPoint extends DataPoint{
	
	private Double[] labels;
	private int label_size;

	public LabeledDataPoint(int dimension,int label_size) {
		super(dimension);
		setLabelSize(label_size);
	}
	
	public LabeledDataPoint(Double[] features,Double[] labels){
		super(features);
		if(labels==null)throw new NullPointerException();
		setLabelSize(labels.length);
		for(int i=0;i<label_size;++i)
			this.labels[i] = labels[i];
	}
	
	public void setLabelSize(int label_size){
		if(label_size<=0)throw new IllegalArgumentException("Label Size must be a strictly pozitive integer");
		labels=new Double[label_size];
		Arrays.fill(labels, 0d);
		this.label_size=label_size;
	}
	
	public int getLabelSize(){
		return label_size;
	}
	
	public Double[] getLabels(){
		return this.labels;
	}
	
	public double getLabel(int i){
		if(i<0||i>this.label_size)throw new IndexOutOfBoundsException();
		if(this.labels[i]==null)throw new IllegalStateException("The data point labels are not set.");
		return this.labels[i];
	}
	
	public void setLabel(int i,double val){
		if(i<0||i>this.label_size)throw new IndexOutOfBoundsException();
		this.labels[i]=val;
	}
	
	public void setLabels(Double...labels){
		if(labels.length!=label_size)throw new IllegalArgumentException("Wrong number of arguments for a LabeledDataPoint with label_size "+label_size);
		for(int i=0;i<labels.length;++i){
			this.labels[i]=labels[i];
		}
	}


}

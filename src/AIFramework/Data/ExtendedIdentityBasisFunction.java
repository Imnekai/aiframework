package AIFramework.Data;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;

public class ExtendedIdentityBasisFunction extends BasisFunction{


	private static final long serialVersionUID = 2990458161042347605L;

	public ExtendedIdentityBasisFunction(int dimension) {
		super(dimension,dimension+1);
	}


	@Override
	protected Matrix<Double> function(Matrix<Double> matrix) {
		Matrix<Double> ret=null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(matrix.rowNr(), this.final_dimension);
		for(int i=0;i<matrix.rowNr();++i)
			for(int j=0;j<matrix.colNr();++j)
				ret.put(i,j,matrix.get(i, j));
		for(int i=0;i<matrix.rowNr();++i)
			ret.put(i,this.final_dimension-1,1d);
		return ret;
	}
	
	public ExtendedIdentityBasisFunction duplicate(){
		ExtendedIdentityBasisFunction ret = new ExtendedIdentityBasisFunction(this.start_dimension);
		return ret;
	}

}

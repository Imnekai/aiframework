package AIFramework.Data;

import AIFramework.Computation.Matrix;

public class RBFKernel extends KernelFunction{

	private static final long serialVersionUID = 9054606772114782096L;
	private double gamma;
	
	public RBFKernel(){
		this.gamma = 2d;
	}
	
	public RBFKernel(double gamma){
		this.gamma=gamma;
	}

	@Override
	protected Double function(Matrix<Double> v1, Matrix<Double> v2) {
		double ret=0;
		for(int i=0;i<v1.size();++i){
			ret+=Math.pow(v1.get(i)-v2.get(i), 2)/gamma;
		}
		ret = Math.exp(-ret);
		return ret;
	}
	
	public RBFKernel duplicate(){
		RBFKernel ret = new RBFKernel();
		ret.gamma = this.gamma;
		return ret;
	}

}

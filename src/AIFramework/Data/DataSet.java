package AIFramework.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Core.Utility;

public class DataSet {
	
	private ArrayList<DataPoint> unlabeled_data;
	private ArrayList<LabeledDataPoint> labeled_data;
	
	private int dimension, label_size;
	
	public DataSet(int dimension){
		if(dimension<=0)throw new IllegalArgumentException("Dimension must be a stricly pozitive integer");
		unlabeled_data=new ArrayList<DataPoint>();
		labeled_data=new ArrayList<LabeledDataPoint>();
		this.dimension = dimension;
		label_size=0;
	}
	
	public DataSet(int dimension,int label_size){
		this(dimension);
		if(label_size<=0)throw new IllegalArgumentException("Label Size must be a stricly pozitive integer");
		this.label_size = label_size;
	}
	
	public DataSet(DataPoint dp){
		this.dimension = dp.getDimension();
		this.unlabeled_data = new ArrayList<DataPoint>();
		labeled_data=new ArrayList<LabeledDataPoint>();
		if(dp.getClass() == LabeledDataPoint.class){
			this.label_size = ((LabeledDataPoint)dp).getLabelSize();
		}
		this.add(dp);
	}
	
	public int getDimension(){
		return this.dimension;
	}
	
	public int getLabelSize(){
		return this.label_size;
	}
	
	public DataSet shuffle(){
		Collections.shuffle(this.unlabeled_data);
		if(this.labeled_data!=null)Collections.shuffle(labeled_data);
		return this;
	}
	
	public void add(DataPoint data_point){
		if(data_point.getDimension()!=this.dimension)
			throw new IllegalArgumentException("DataPoint dimension does not match the DataSet dimension");
		
		if(data_point.getClass()==LabeledDataPoint.class){
			
			if(((LabeledDataPoint)data_point).getLabelSize()!=this.getLabelSize())
				throw new IllegalArgumentException("LabeledDataPoint label size does not match the DataSet label size");
				this.labeled_data.add((LabeledDataPoint)data_point);
		}
		else
		{
			this.unlabeled_data.add(data_point);
		}
	}
	
	public Matrix<Double> getFeatureMatrix(){
		Matrix<Double> ret=null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(this.getDataPoints());
		return ret;
	}
	
	public Matrix<Double> getLabeledFeatureMatrix(){
		Matrix<Double> ret=null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(this.getLabeledData());
		return ret;
	}
	
	public Matrix<Double> getUnlabeledFeatureMatrix(){
		Matrix<Double> ret=null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(this.getUnlabeledData());
		return ret;
	}
	
	public Matrix<Double> getLabelMatrix(){
		Matrix<Double> ret=null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(this.labeled_data==null||this.labeled_data.size()==0)throw new UnsupportedOperationException("No labeled data");
		ret.set(this.labeled_data.size(), this.label_size);
		for(int i=0;i<ret.rowNr();++i)
			for(int j=0;j<ret.colNr();++j)
				ret.put(i, j,this.labeled_data.get(i).getLabel(j));
		return ret;
	}
	
	public int size(){
		int ret=0;
		if(this.labeled_data!=null)ret+=this.labeled_data.size();
		if(this.unlabeled_data!=null)ret+=this.unlabeled_data.size();
		return ret;
	}
	

	
	public void encode1ofK(){
		if(this.labeled_data==null||this.labeled_data.size()==0)throw new NullPointerException("No labeled data to encode");
		if(this.label_size!=1)throw new IllegalStateException("Data must have exactly one label");
		HashMap<Double,Integer> hash=new HashMap<Double,Integer>();
		for(LabeledDataPoint dp:this.labeled_data){
			if(!hash.containsKey(dp.getLabel(0)))
				hash.put(dp.getLabel(0), hash.size());
		}
		this.label_size = hash.size();
		for(LabeledDataPoint dp:this.labeled_data){
			int pos = hash.get(dp.getLabel(0));
			dp.setLabelSize(hash.size());
			dp.setLabel(pos, 1d);
		}
	}
	
	public ArrayList<DataPoint> getDataPoints(){
		ArrayList<DataPoint> ret=new ArrayList<DataPoint>();
		for(DataPoint dp:this.labeled_data)
			ret.add(dp);
		for(DataPoint dp:this.unlabeled_data)
			ret.add(dp);
		return ret;
	}
	
	public ArrayList<DataPoint> getUnlabeledData(){
		return this.unlabeled_data;
	}
	
	public ArrayList<LabeledDataPoint> getLabeledData(){
		return this.labeled_data;
	}
	
	public static DataSet generateBlobs(double[][] centers,int[] labels,int per_center,double dist){
		if(centers==null || centers.length==0)throw new IllegalArgumentException("A list of centroids must be given");
		int dims = centers[0].length;
		if(labels==null||labels.length!=centers.length)throw new IllegalArgumentException("Each centrois must have exacly one label");
		DataSet ret=new DataSet(dims,1);
		for(int i=0;i<centers.length;++i){
			for(int k=0;k<per_center;++k){
				LabeledDataPoint dp = new LabeledDataPoint(dims,1);
				dp.setLabels((double)labels[i]);
				for(int j=0;j<dims;++j)
					dp.setFeature(j, Utility.rand(centers[i][j]-dist, centers[i][j]+dist));
				ret.add(dp);
			}
		}
		
		
		return ret;
	}

}

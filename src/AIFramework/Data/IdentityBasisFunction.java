package AIFramework.Data;

import AIFramework.Computation.Matrix;

public class IdentityBasisFunction extends BasisFunction{

	private static final long serialVersionUID = 9200234986755294809L;

	public IdentityBasisFunction(int dimension) {
		super(dimension,dimension);
	}


	@Override
	protected Matrix<Double> function(Matrix<Double> matrix) {
		return matrix.duplicate();
	}
	
	public IdentityBasisFunction duplicate(){
		IdentityBasisFunction ret = new IdentityBasisFunction(this.start_dimension);
		return ret;
	}

}

package AIFramework.Data;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;

public class PolynomialBasisFunction extends BasisFunction{

	private static final long serialVersionUID = -5353397317526230197L;

	public PolynomialBasisFunction(int degree) {
		super(1, degree+1);
	}


	@Override
	protected Matrix<Double> function(Matrix<Double> matrix) {
		Matrix<Double> ret=null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(matrix.rowNr(),this.final_dimension);
		for(int i=0;i<matrix.rowNr();++i){
			double x = 1d;
			for(int j=0;j<this.final_dimension;++j){
				ret.put(i, j,x);
				x*=matrix.get(i);
			}
		}
		return ret;
	}
	
	public PolynomialBasisFunction duplicate(){
		PolynomialBasisFunction ret = new PolynomialBasisFunction(this.final_dimension-1);
		return ret;
	}

}

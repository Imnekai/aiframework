package AIFramework.Data;

import java.io.Serializable;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Utility;

public class Preprocessor implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2318593296531854412L;
	private Matrix<Double> meanVector=null;
	private Matrix<Double> absVector=null;
	
	private boolean normalize,center;
	private int nr_components;
	private Matrix<Double> svd_v=null;
	
	public Preprocessor(){
		this.normalize = false;
		this.center=false;
		this.nr_components = 0;
	}
	
	public Matrix<Double> process(DataPoint data_point){
		return process(data_point.getFeatureVector());
	}
	
	/**
	 * 
	 * @param matrix
	 * @return New matrix with the processed content of the original one
	 */
	public Matrix<Double> process(Matrix<Double> matrix){
		Matrix<Double> ret = matrix.duplicate();
		ret = this.processi(ret);
		return ret;
	}
	
	/**
	 * Process matrix in place
	 * @param matrix
	 * @return Reference to the original matrix
	 */
	public Matrix<Double> processi(Matrix<Double> matrix){
		if(this.center)
			matrix.subiRows(this.meanVector);
		if(this.normalize)
			matrix.diviRows(this.absVector);
		if(this.nr_components!=0)
		    matrix.muli(svd_v);
		return matrix;
	}
	
	public void setNormalize(boolean normalize){
		this.normalize=normalize;
	}
	
	public boolean getNormalize(){
		return this.normalize;
	}
	
	public void setCenter(boolean center){
		this.center=center;
	}
	
	public boolean getCenter(){
		return this.center;
	}
	
	public void setPCA(int nr_components){
		if(nr_components<0)throw new IllegalArgumentException("The number of components can't be a negative integer");
		this.nr_components=nr_components;
		if(this.nr_components==0)
			this.svd_v = null;
		else
			this.center=true;
	}
	
	public int getPCA(){
		return this.nr_components;
	}
	
	public void setParams(Matrix<Double> matrix){
		if(this.center)
			this.meanVector = matrix.getColumnMeans();
		if(this.normalize)
			this.absVector = matrix.getColumnsAbs();
		if(this.nr_components!=0)
			this.svd_v = matrix.transpose().mul(matrix).muli(1.0d/matrix.rowNr()).getSVD()[2].getColumns(Utility.firstK(nr_components));
		
	}
	
	public Preprocessor duplicate(){
		Preprocessor ret = new Preprocessor();
		if(this.absVector!=null)
			ret.absVector = this.absVector.duplicate();
		ret.center = this.center;
		if(this.meanVector!=null)
			ret.meanVector = this.meanVector.duplicate();
		ret.normalize = this.normalize;
		ret.nr_components = this.nr_components;
		if(this.svd_v!=null)
			ret.svd_v = this.svd_v.duplicate();
		return ret;
	}

}

package AIFramework.Data;

import java.util.Arrays;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;

public class DataPoint {
	
	private Double[] features;
	private int dimension;
	
	public DataPoint(int dimension){
		setDimension(dimension);
		
	}
	
	public DataPoint(Double[] features){
		if(features==null)throw new NullPointerException();
		setDimension(features.length);
		for(int i=0;i<dimension;++i)
			this.features[i]=features[i];
	}
	
	private void setDimension(int dimension){
		if(dimension<=0)throw new IllegalArgumentException("Dimension must be a stricly pozitive integer");
		this.dimension=dimension;
		features = new Double[dimension];
	}
	
	public void setFeatures(Double...features){
		if(features.length!=dimension)throw new IllegalArgumentException("Wrong number of arguments for a DataPoint with dimension "+dimension);
		for(int i=0;i<features.length;++i){
			this.features[i]=features[i];
		}
	}
	
	public Double[] getFeatures(){
		return this.features;
	}
	
	public Matrix<Double> getFeatureVector(){
		Matrix<Double> ret =null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(1, this.getDimension());
		for(int i=0;i<this.getDimension();++i)
			ret.put(0, i,this.getFeature(i));
		return ret;
	}
	
	
	
	public double getFeature(int i){
		if(i<0||i>this.features.length)throw new IndexOutOfBoundsException();
		if(this.features[i]==null)throw new IllegalStateException("The data point features are not set");
		return this.features[i];
	}
	
	public void setFeature(int i,double val){
		if(i<0||i>this.features.length)throw new IndexOutOfBoundsException();
		this.features[i] = val;
	}
	
	public int getDimension(){
		return dimension;
	}
	
}

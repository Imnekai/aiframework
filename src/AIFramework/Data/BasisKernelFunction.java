package AIFramework.Data;

import java.util.ArrayList;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;

public class BasisKernelFunction extends BasisFunction{
	

	private static final long serialVersionUID = 1635826221931218317L;
	private Matrix<Double> vectors;
	private KernelFunction kernel;
	
	public BasisKernelFunction(Matrix<Double> vectors,KernelFunction kernel){
		super(vectors.colNr(),vectors.rowNr());
		this.vectors=vectors.duplicate();
		this.kernel=kernel;
	}
	
	public <T extends DataPoint> BasisKernelFunction(ArrayList<T> data_points,KernelFunction kernel) throws InstantiationException, IllegalAccessException{
		this(Config.MatrixClass.newInstance().set(data_points),kernel);
	}
	
	public BasisKernelFunction(DataSet data_set,KernelFunction kernel) throws InstantiationException, IllegalAccessException{
		this(data_set.getDataPoints(),kernel);
	}

	@Override
	protected Matrix<Double> function(Matrix<Double> matrix) {
		Matrix<Double> ret = null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(matrix.rowNr(),this.final_dimension);
		for(int i=0;i<matrix.rowNr();++i)
			for(int j=0;j<vectors.rowNr();++j)
				ret.put(i, j, this.kernel.call(matrix.getRow(i), vectors.getRow(j)));
		return ret;
	}
	
	public BasisKernelFunction duplicate(){
		BasisKernelFunction ret = new BasisKernelFunction(this.vectors.duplicate(),this.kernel.duplicate());
		return ret;
	}

}

package AIFramework.UnsupervisedModels;

import java.util.ArrayList;
import java.util.Hashtable;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Core.LearningModel;
import AIFramework.Core.Utility;
import AIFramework.Data.DataSet;

public class KMeansClustering extends UnsupervisedModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4743637703822343604L;
	
	protected int k;
	protected Matrix<Double> centroids=null;
	protected int max_iters=10000;
	protected boolean verbose = false;
	
	public KMeansClustering(int k){
		this.k=k;
	}
	
	
	public int getK(){
		return this.k;
	}
	
	public int getMaxIters(){
		return this.max_iters;
	}
	
	public void setMaxIters(int max_iters){
		this.max_iters = max_iters;
	}
	
	public void setVerbose(boolean verbose){
		this.verbose = verbose;
	}
	
	public Matrix<Double> getCentroids(){
		return this.centroids;
	}

	@Override
	protected void trainInternal(Matrix<Double> design_matrix) {
		if(centroids == null){
			centroids = Matrix.newInstance(k,design_matrix.colNr());
			Matrix<Double> col_abs =design_matrix.getColumnsAbs();
			for(int i=0;i<centroids.rowNr();++i)
				for(int j=0;j<centroids.colNr();++j)
					centroids.put(i, j, Utility.rand(-col_abs.get(j), col_abs.get(j)));
		}
		int it=0;
		boolean change = true;
		ArrayList<ArrayList<Integer>> classes_ind = new ArrayList<ArrayList<Integer>>();
		for(int i=0;i<k;++i)
			classes_ind.add(new ArrayList<Integer>());
		while(it<this.max_iters&&change==true){
			change=false;
			
			double total_dist = 0d;
			for(ArrayList<Integer> inds:classes_ind)
				inds.clear();
			for(int i=0;i<design_matrix.rowNr();++i){
				int centroid = 0;
				double min_dist = design_matrix.getRow(i).dist2(centroids.getRow(0));
				for(int j=1;j<centroids.rowNr();++j){
					double dist = design_matrix.getRow(i).dist2(centroids.getRow(j));
					if(dist<min_dist){
						min_dist=dist;
						centroid = j;
					}
				}
				total_dist+=min_dist;
				classes_ind.get(centroid).add(i);
			}
			for(int i=0;i<k;++i){
				int[] inds = Utility.toArray(classes_ind.get(i));
				if(inds==null)continue;
				Matrix<Double> new_centroid = design_matrix.getRows(inds).getColumnMeans();
				if(new_centroid.dist2(centroids.getRow(i))>Config.EPSILON)
					change=true;
				centroids = centroids.putRow(i, new_centroid);
			}
			if(verbose)
				System.out.println(String.format("Iteration: %d Total distance: %f", it,total_dist));
			it++;
		}
		
	}

	@Override
	protected Matrix<Double> predictInternal(Matrix<Double> design_matrix) {
		Matrix<Double> ret = Matrix.newInstance(design_matrix.rowNr(),1);
		for(int i=0;i<ret.rowNr();++i){
			double min_dist = centroids.getRow(0).dist2(design_matrix.getRow(i));
			int centroid = 0;
			for(int j=0;j<centroids.rowNr();++j){
				double dist = centroids.getRow(j).dist2(design_matrix.getRow(i));
				if(dist<min_dist){
					min_dist=dist;
					centroid = j;
				}
			}
			ret.put(i, (double) centroid);
		}
		return ret;
	}
	
	public double test(DataSet test_set){
		double ret=0d;
		Matrix<Double> pred = this.predict(test_set);
		Matrix<Double> labels = test_set.getLabelMatrix();
		Hashtable<Double,Double> volume = new Hashtable<Double,Double>();
		for(int i=0;i<labels.size();++i)
			if(volume.containsKey(labels.get(i)))
				volume.put(labels.get(i), volume.get(labels.get(i))+1d);
			else
				volume.put(labels.get(i), 1d);
		for(int i=0;i<pred.size()-1;++i)
			for(int j=i+1;j<pred.size();++j)
				if(!Utility.areEqual(pred.get(i),pred.get(j))&&Utility.areEqual(labels.get(i),labels.get(j)))
					ret+=1d/volume.get(labels.get(i));
				else if(Utility.areEqual(pred.get(i),pred.get(j))&&!Utility.areEqual(labels.get(i),labels.get(j)))
					ret+=1d/volume.get(labels.get(j));
		return ret;
	}
	
	@Override
	public LearningModel duplicate() {
		// TODO Auto-generated method stub
		return null;
	}

}

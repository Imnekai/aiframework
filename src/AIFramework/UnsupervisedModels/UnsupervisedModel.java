package AIFramework.UnsupervisedModels;

import AIFramework.Computation.Matrix;
import AIFramework.Core.LearningModel;
import AIFramework.Data.BasisKernelFunction;
import AIFramework.Data.DataSet;
import AIFramework.Data.IdentityBasisFunction;

public abstract class UnsupervisedModel extends LearningModel{

	@Override
	public void train(DataSet train_set) {
		train(train_set.getFeatureMatrix());
	}
	
	public void train(Matrix<Double> data_points){
		this.preprocessor.processi(data_points);
		if(kernel!=null)
			this.basis_function = new BasisKernelFunction(data_points,kernel);
		if(this.basis_function==null)
			this.basis_function=new IdentityBasisFunction(data_points.colNr());
		Matrix<Double> design_matrix = this.basis_function.call(data_points); 
		this.trainInternal(design_matrix);
	}
	
	protected abstract void trainInternal(Matrix<Double> design_matrix);


	@Override
	public abstract LearningModel duplicate();

}

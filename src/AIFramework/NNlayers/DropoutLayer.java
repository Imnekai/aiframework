package AIFramework.NNlayers;

import AIFramework.Computation.HardlimFunction;
import AIFramework.Computation.Matrix;

public class DropoutLayer extends NNlayer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3518545357565126889L;
	protected double drop_prob = 0.5d;
	protected HardlimFunction hardlim;
	protected Matrix<Double> drop_mask = null;
	
	public DropoutLayer(double droup_prob){
		this.setDropProbability(droup_prob);
	}
	
	public DropoutLayer(){
		this.setDropProbability(0.5d);
	}
	
	public void setDropProbability(double drop_prob){
		if(drop_prob<0d||drop_prob>1d)
			throw new IllegalArgumentException("Probability must be between 0 and 1");
		this.drop_prob = drop_prob;
		this.hardlim = new HardlimFunction(drop_prob);
	}
	
	public double getDropProbability(){
		return this.drop_prob;
	}

	@Override
	public Matrix<Double> forward(Matrix<Double> input, boolean update) {
		if(!update)
			return input;
		
		if(drop_mask==null||!drop_mask.match(input))
			drop_mask = Matrix.newInstance(input.rowNr(), input.colNr());
		
		drop_mask = hardlim.call(drop_mask.rand());
		
		return input.emuli(drop_mask);
	}

	@Override
	public Matrix<Double> backward(Matrix<Double> gradient, boolean update) {
		if(!update)
			return gradient;
		
		return gradient.emuli(drop_mask);
	}

	@Override
	public DropoutLayer duplicate() {
		DropoutLayer ret = new DropoutLayer();
		ret.drop_prob = this.drop_prob;
		if(this.drop_mask!=null)
			ret.drop_mask = this.drop_mask.duplicate();
		return ret;
	}

}

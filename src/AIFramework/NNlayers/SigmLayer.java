package AIFramework.NNlayers;

import AIFramework.Computation.SigmFunction;

public class SigmLayer extends TransferLayer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2542259847672398570L;

	public SigmLayer() {
		super(new SigmFunction().fixFlatSpot(true));
	}

}

package AIFramework.NNlayers;

import AIFramework.Computation.Function;
import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;

public class TransferLayer extends NNlayer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4263727284076396365L;
	protected Matrix<Double> input_cache;
	protected Function<Double> transfer_function;
	
	public TransferLayer(Function<Double> transfer_function){
		this.transfer_function = transfer_function;
	}

	@Override
	public Matrix<Double> forward(Matrix<Double> input, boolean update) {
		if(update)
			this.input_cache = input;
		return this.transfer_function.call(input);
	}

	@Override
	public Matrix<Double> backward(Matrix<Double> gradient, boolean update) {
		return gradient.emuli(this.transfer_function.derivative(this.input_cache));
	}

	@Override
	public NNlayer duplicate() {
		return new TransferLayer(this.transfer_function.duplicate());
	}
	
	public Matrix<Double> aproxGrad(Matrix<Double> input){
		Matrix<Double> ret = null;
		try {
			ret= Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret.set(input.rowNr(), input.colNr());
		double t = Config.EPSILON;
		for(int i=0;i<input.size();++i){
			input.put(i,input.get(i)+t);
			double f1 = this.forward(input, false).get(i);
			input.put(i,input.get(i)-2*t);
			double f2 = this.forward(input, false).get(i);
			input.put(i,input.get(i)+t);
			ret.put(i, (f1-f2)/(2*t));
		}
		return ret;
	}

}

package AIFramework.NNlayers;

import AIFramework.Computation.ReluFunction;

public class ReluLayer extends TransferLayer{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8798483231906712764L;

	public ReluLayer() {
		super(new ReluFunction());
	}

}

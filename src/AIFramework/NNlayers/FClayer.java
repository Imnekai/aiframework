package AIFramework.NNlayers;

import AIFramework.Computation.FunctionDouble;
import AIFramework.Computation.Matrix;
import AIFramework.Computation.ReluFunction;
import AIFramework.Core.Config;
import AIFramework.Core.Utility;

public class FClayer extends NNlayer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2591910285880512554L;
	
	protected Matrix<Double> weights,weights_velocity=null;
	protected Matrix<Double> bias,bias_velocity=null;
	protected Matrix<Double> input_cache;
	protected double learning_rate=1.0E-3;
	protected double regularization=0.1d;
	protected double momentum = 0d;
	protected boolean keep_gradients=false;
	protected boolean has_bias;
	
	protected Matrix<Double> wgrad_chache=null,bgrad_chache=null;
	
	public FClayer(int input_size,int size,boolean has_bias){
		this.has_bias = has_bias;
		weights = Matrix.newInstance();
		weights.set(input_size, size);
		for(int i=0;i<weights.size();++i)
				weights.put(i,Utility.rand(-1, 1));
		if(has_bias){
			bias = Matrix.newInstance();
			bias.set(1,size);
			for(int i=0;i<bias.size();++i)
				bias.put(i,Utility.rand(-1, 1));
		}
	}
	
	public FClayer setMomentum(double momentum){
		this.momentum = momentum;
		if(this.momentum!=0&&this.weights_velocity==null){
			this.weights_velocity = Matrix.newInstance(this.weights.rowNr(),this.weights.colNr());
			this.weights_velocity.fill(0d);
			if(this.has_bias){
				this.bias_velocity = Matrix.newInstance(this.bias.rowNr(),this.bias.colNr());
				this.bias_velocity.fill(0d);
			}
		}
		else if(this.momentum==0){
			this.weights_velocity = null;
			this.bias_velocity = null;
		}
		return this;
	}
	
	public double getMomentum(){
		return this.momentum;
	}
	
	public FClayer setLearningRate(double lr){
		this.learning_rate = lr;
		return this;
	}
	
	public double getLearningRate(){
		return this.learning_rate;
	}
	
	
	public FClayer setRegularization(double reg){
		this.regularization = reg;
		return this;
	}
	
	public double getRegularization(){
		return this.regularization;
	}
	
	public Matrix<Double> getWeights(){
		return this.weights;
	}
	
	public Matrix<Double> getBias(){
		return this.bias;
	}
	
	public void setKeepGradients(boolean val){
		this.keep_gradients = val;
	}
	
	public Matrix<Double> getWeightsGrad(){
		return this.wgrad_chache;
	}
	
	public Matrix<Double> getBiasGrad(){
		return this.bgrad_chache;
	}
	
	/**
	 * 
	 * @return Number of neurons in the layer
	 */
	public int getSize(){
		return this.weights.colNr();
	}
	
	/**
	 * 
	 * @return Accepted size of the input
	 */
	public int getInputSize(){
		return this.weights.rowNr();
	}
	

	@Override
	public Matrix<Double> forward(Matrix<Double> input, boolean update) {
		if(input.colNr()!=this.weights.rowNr()) throw new IllegalArgumentException("Input does not match the expected size");
		Matrix<Double> output = null;
		if(this.has_bias)
			output = input.mul(this.weights).addiRows(this.bias);
		else
			output = input.mul(this.weights);
		if(update)
			this.input_cache = input;

		//if(this.has_bias)
		//	System.out.println(input);
		return output;
	}
	
	@Override 
	public Matrix<Double> backward(Matrix<Double> gradient, boolean update){
		Matrix<Double> w_grad = input_cache.transpose().mul(gradient).addi(this.weights.mul(regularization));
		Matrix<Double> ones = Matrix.newInstance();
		ones.set(1,gradient.rowNr());
		ones.fill(1d);
		Matrix<Double> b_grad = null;
		if(this.has_bias){
			b_grad = ones.mul(gradient).addi(this.bias.mul(regularization));
		}
		Matrix<Double> in_grad = gradient.mul(weights.transpose());
		
		if(this.keep_gradients){
			this.wgrad_chache = w_grad.duplicate();
			if(this.has_bias){
				this.bgrad_chache = b_grad.duplicate();
			}
		}
		
		if(update){
			if(this.momentum==0){
				this.weights.subi(w_grad.muli(learning_rate));
				if(this.has_bias)
					this.bias.subi(b_grad.muli(learning_rate));
			}
			else{
				this.weights_velocity.muli(this.momentum).subi(w_grad.muli(learning_rate));
				this.weights.addi(weights_velocity);
				
				if(this.has_bias){
					this.bias_velocity.muli(this.momentum).subi(b_grad.muli(learning_rate));
					this.bias.addi(this.bias_velocity);
				}
			}
		}
		
		return in_grad;
	}
	
	public Matrix<Double> aproxWgrad(Matrix<Double> input){
		Matrix<Double> ret= Matrix.newInstance();
		ret.set(this.weights.rowNr(),this.weights.colNr());
		double t = Config.EPSILON;
		for(int i=0;i<weights.size();++i){
			weights.put(i, weights.get(i)+t);
			double f1 = this.forward(input,true).getColumn(i%weights.colNr()).getSum();
			weights.put(i, weights.get(i)-2*t);
			double f2 = this.forward(input,true).getColumn(i%weights.colNr()).getSum();
			ret.put(i,(f1-f2)/(2*t));
			weights.put(i, weights.get(i)+t);
		}
		return ret;
	}
	
	public Matrix<Double> aproxBgrad(Matrix<Double> input){
		Matrix<Double> ret= Matrix.newInstance();
		ret.set(1,this.bias.size());
		double t = Config.EPSILON;
		for(int i=0;i<bias.size();++i){
			bias.put(i, bias.get(i)+t);
			double f1 = this.forward(input,true).getColumn(i).getSum();
			bias.put(i, bias.get(i)-2*t);
			double f2 = this.forward(input,true).getColumn(i).getSum();
			ret.put(i,(f1-f2)/(2*t));
			bias.put(i, bias.get(i)+t);
		}
		return ret;
	}
	
	public FClayer duplicate(){
		FClayer ret=new FClayer(this.getSize(),this.getInputSize(),this.has_bias);
		ret.weights=this.weights.duplicate();
		if(this.has_bias)
			ret.bias=this.bias.duplicate();
		ret.input_cache=this.input_cache.duplicate();
		ret.learning_rate=this.learning_rate;
		ret.regularization=this.regularization;
		ret.keep_gradients=this.keep_gradients;
		ret.momentum = this.momentum;
		if(this.wgrad_chache!=null)
			ret.wgrad_chache=this.wgrad_chache.duplicate();
		if(this.bgrad_chache!=null)
			ret.bgrad_chache=this.bgrad_chache.duplicate();
		if(this.weights_velocity!=null)
			ret.weights_velocity=this.weights_velocity.duplicate();
		if(this.bias_velocity!=null)
			ret.bias_velocity=this.bias_velocity.duplicate();
		return ret;
	}
	
	@Override
	public double getRegLoss(){
		if(this.has_bias)
			return (this.weights.getL2Loss()+this.bias.getL2Loss())*this.regularization*0.5d;
		else
			return (this.weights.getL2Loss())*this.regularization*0.5d;
	}

}

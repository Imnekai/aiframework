package AIFramework.NNlayers;

import java.io.Serializable;

import AIFramework.Computation.Matrix;

public abstract class NNlayer implements Serializable{
	
	/**
	 * Forward pass
	 * @param input - The input matrix
	 * @param update - Whether it's an update pass or not 
	 * @return The layer output matrix
	 */
	public abstract Matrix<Double> forward(Matrix<Double> input, boolean update);
	
	/**
	 * Backward pass
	 * @param gradient
	 * @param update - Whether to update the parameters based on the gradients or not
	 * @return The gradient of the input
	 */
	public abstract Matrix<Double> backward(Matrix<Double> gradient,boolean update);
	
	public double getRegLoss(){
		return 0d;
	}
	
	public abstract NNlayer duplicate();

}

package AIFramework.Computation;

import java.util.ArrayList;

import AIFramework.Core.Config;
import AIFramework.Core.Utility;
import AIFramework.Data.DataPoint;

public class NaiveMatrix extends Matrix<Double>{
	

	private static final long serialVersionUID = -6604804196990602698L;
	private Double[][] matrix;
	private int rows_nr,cols_nr;
	
	@Override
	public NaiveMatrix identity(int n){
		NaiveMatrix identity = new NaiveMatrix(n,n);
		identity.fill(0d);
		for(int i=0;i<n;++i)identity.matrix[i][i]=1d;
		return identity;
	}
	
	public NaiveMatrix(int r,int c){
		set(r,c);
	}
	
	public NaiveMatrix(){
		
	}

	@Override
	public NaiveMatrix mul(Matrix<Double> b) throws ArithmeticException {
		if(this.cols_nr!=b.rowNr())throw new ArithmeticException("Wrong matrix shape");
		NaiveMatrix c = new NaiveMatrix(this.rows_nr,b.colNr());
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<b.colNr();++j)
			{
				c.matrix[i][j]=0d;
				for(int k=0;k<this.cols_nr;++k)
					c.matrix[i][j]+=this.matrix[i][k]*b.get(k, j);
			}
		return c;
	}

	@Override
	public NaiveMatrix mul(Double a) {
		NaiveMatrix ret=new NaiveMatrix(this.rowNr(),this.colNr());
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				ret.matrix[i][j] = this.matrix[i][j]*a;
		return ret;
	}

	@Override
	public NaiveMatrix add(Matrix<Double> b) throws ArithmeticException {
		if(b.rowNr()!=this.rowNr()||b.colNr()!=this.colNr())throw new ArithmeticException();
		NaiveMatrix ret = new NaiveMatrix(rows_nr,cols_nr);
		for(int i=0;i<rows_nr;++i)
			for(int j=0;j<cols_nr;++j)
				ret.put(i, j, this.get(i, j)+b.get(i,j));
		return ret;
	}

	@Override
	public NaiveMatrix transpose() {
		NaiveMatrix ret=new NaiveMatrix(cols_nr,rows_nr);
		for(int i=0;i<rows_nr;++i)
			for(int j=0;j<cols_nr;++j)
				ret.matrix[j][i]=matrix[i][j];
		return ret;
	}

	@Override
	public NaiveMatrix inverse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int colNr() {
		return cols_nr;
	}

	@Override
	public int rowNr() {
		return rows_nr;
	}

	@Override
	public NaiveMatrix getRow(int row_nr) throws IndexOutOfBoundsException {
		NaiveMatrix ret = new NaiveMatrix(1,this.cols_nr);
		for(int i=0;i<this.cols_nr;++i)
			ret.put(i, this.get(row_nr, i));
		return ret;
	}

	@Override
	public NaiveMatrix getColumn(int column) throws IndexOutOfBoundsException {
		NaiveMatrix ret = new NaiveMatrix(this.rows_nr,1);
		for(int i=0;i<this.rows_nr;++i)
			ret.put(i, this.get(i,column));
		return ret;
	}

	@Override
	public Double get(int row, int col) {
		if(row<0||row>=rows_nr||col<0||col>cols_nr) throw new IndexOutOfBoundsException();
		return matrix[row][col];
	}

	@Override
	public void put(int row, int col,Double val) throws IndexOutOfBoundsException {
		if(row<0||row>=rows_nr||col<0||col>cols_nr) throw new IndexOutOfBoundsException();
		matrix[row][col] = val;
	}
	

	public String toString(){
		String ret="";
		for(int i=0;i<rows_nr;++i)
		{
			for(int j=0;j<cols_nr;++j)ret+=get(i,j)+" ";
			ret+=System.lineSeparator();
		}
		return ret;
	}

	@Override
	public void set(int rows, int columns) {
		rows_nr=rows;
		cols_nr=columns;
		matrix = new Double[rows][columns];
		
	}

	@Override
	public boolean isEqual(Matrix<Double> b) throws ArithmeticException{
		if(this.rowNr()!=b.rowNr()||this.colNr()!=b.colNr())throw new ArithmeticException("Matrices of different shape");
		for(int i=0;i<this.rowNr();++i)
			for(int j=0;j<this.colNr();++j)
				if(Math.abs(this.get(i,j)-b.get(i, j))>Config.EPSILON)return false;
		return true;
	}

	@Override
	public <C extends DataPoint> Matrix<Double> set(ArrayList<C> data_points) {
		if(data_points==null||data_points.size()==0)throw new UnsupportedOperationException("No data points");
		this.matrix=new Double[data_points.size()][data_points.get(0).getDimension()];
		this.rows_nr = data_points.size();
		this.cols_nr = data_points.get(0).getDimension();
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				matrix[i][j] = data_points.get(i).getFeature(j);
		return this;
	}

	@Override
	public NaiveMatrix duplicate() {
		NaiveMatrix ret = new NaiveMatrix(this.rows_nr,this.cols_nr);
		for(int i=0;i<this.size();++i)
			ret.put(i, this.get(i));
		return ret;
	}

	@Override
	public NaiveMatrix getColumnMeans() {
		NaiveMatrix ret = this.getColumnSum();
		return ret.muli(1d/this.rows_nr);
	}

	@Override
	public NaiveMatrix getColumnsAbs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NaiveMatrix getRowsAbs() {
		return null;
	}
	
	@Override
	public NaiveMatrix diviRows(Matrix<Double> vector) {
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.matrix[i][j]/=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix subiRows(Matrix<Double> vector) {
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.matrix[i][j]-=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix muli(Matrix<Double> b) {
		Double[][] mat = new Double[this.rows_nr][b.colNr()];
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<b.colNr();++j){
				mat[i][j]=0d;
				for(int k=0;k<this.cols_nr;++k)
					mat[i][j]+=this.matrix[i][k]*b.get(k,j);
			}
		this.matrix = mat;
		this.cols_nr=b.colNr();
		return this;
				
	}

	@Override
	public NaiveMatrix muli(Double a) {
		for(int i=0;i<this.size();++i)
			this.put(i, this.get(i)*a);
		return this;
	}

	@Override
	public NaiveMatrix addi(Matrix<Double> b) {
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.matrix[i][j]+=b.get(i, j);
		return this;
	}

	@Override
	public NaiveMatrix getRowMaxs() {
		NaiveMatrix ret=new NaiveMatrix(this.rows_nr,1);
		for(int i=0;i<this.rows_nr;++i){
			ret.put(i,this.get(i, 0));
			for(int j=1;j<this.cols_nr;++j)
				if(ret.get(i)<this.get(i,j))
					ret.put(i, this.get(i, j));
		}
		return ret;
	}

	@Override
	public NaiveMatrix getRowMaxsInd() {
		NaiveMatrix ret=new NaiveMatrix(this.rows_nr,1);
		for(int i=0;i<this.rows_nr;++i){
			double mx=this.get(i,0);
			ret.put(i,0d);
			for(int j=1;j<this.cols_nr;++j)
				if(ret.get(i)<this.get(i,j)){
					ret.put(i, (double) j);
					mx = this.get(i,j);
				}
		}
		return ret;
	}

	@Override
	public NaiveMatrix concatH(Matrix<Double> b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NaiveMatrix concatV(Matrix<Double> b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NaiveMatrix addiRows(Matrix<Double> vector) {
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.matrix[i][j]+=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix emuli(Matrix<Double> b) {
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.matrix[i][j]*=b.get(i, j);
		return this;
	}

	@Override
	public NaiveMatrix emul(Matrix<Double> b) {
		NaiveMatrix ret = this.duplicate();
		return ret.emuli(b);
	}

	@Override
	public NaiveMatrix sub(Matrix<Double> b) {
		NaiveMatrix ret = this.duplicate();
		return ret.subi(b);
	}

	@Override
	public NaiveMatrix subi(Matrix<Double> b) {
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.matrix[i][j]-=b.get(i, j);
		return this;
	}

	@Override
	public Double getSum() {
		Double ret = 0d;
		for(int i=0;i<this.size();++i)
			ret+=this.get(i);
		return ret;
	}

	@Override
	public NaiveMatrix getRowSum() {
		NaiveMatrix ret=new NaiveMatrix(this.rows_nr,1);
		for(int i=0;i<this.rows_nr;++i){
			ret.put(i,0d);
			for(int j=0;j<this.cols_nr;++j)
					ret.matrix[i][0]+=this.matrix[i][j];	
		}
		return ret;
	}

	@Override
	public NaiveMatrix getColumnSum() {
		NaiveMatrix ret=new NaiveMatrix(1,this.cols_nr);
		for(int i=0;i<this.cols_nr;++i){
			ret.put(i,0d);
			for(int j=0;j<this.rows_nr;++j)
					ret.matrix[0][i]+=this.matrix[j][i];	
		}
		return ret;
	}

	

	@Override
	public NaiveMatrix getRows(int[] indices) {
		NaiveMatrix ret = new NaiveMatrix(indices.length,this.cols_nr);
		for(int i=0;i<indices.length;++i)
			for(int j=0;j<this.cols_nr;++j)
				ret.matrix[i][j] = this.matrix[indices[i]][j];
		return ret;
	}

	@Override
	public NaiveMatrix getColumns(int[] indices) {
		NaiveMatrix ret = new NaiveMatrix(this.rows_nr,indices.length);
		for(int i=0;i<indices.length;++i)
			for(int j=0;j<this.rows_nr;++j)
				ret.matrix[j][i] = this.matrix[j][indices[i]];
		return ret;
	}

	@Override
	public NaiveMatrix rand() {
		for(int i=0;i<this.size();++i)
			this.put(i, Utility.rand(0d, 1d));
		return this;
	}

	@Override
	public NaiveMatrix[] getSVD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NaiveMatrix subRows(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.subiRows(vector);
		return ret;
	}

	@Override
	public NaiveMatrix subiColumns(Matrix<Double> vector) {
		for(int i=0;i<this.cols_nr;++i)
			for(int j=0;j<this.rows_nr;++j)
				this.matrix[j][i]-=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix subColumns(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.subiColumns(vector);
		return ret;
	}

	@Override
	public NaiveMatrix addRows(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.addiRows(vector);
		return ret;
	}

	@Override
	public NaiveMatrix addiColumns(Matrix<Double> vector) {
		for(int i=0;i<this.cols_nr;++i)
			for(int j=0;j<this.rows_nr;++j)
				this.matrix[j][i]+=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix addColumns(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.addiColumns(vector);
		return ret;
	}

	@Override
	public NaiveMatrix divRows(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.diviRows(vector);
		return ret;
	}

	@Override
	public NaiveMatrix diviColumns(Matrix<Double> vector) {
		for(int i=0;i<this.cols_nr;++i)
			for(int j=0;j<this.rows_nr;++j)
				this.matrix[j][i]/=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix divColumns(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.diviColumns(vector);
		return ret;
	}

	@Override
	public NaiveMatrix mulRows(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.muliRows(vector);
		return ret;
	}

	@Override
	public NaiveMatrix muliRows(Matrix<Double> vector) {
		for(int i=0;i<this.rows_nr;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.matrix[i][j]*=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix muliColumns(Matrix<Double> vector) {
		for(int i=0;i<this.cols_nr;++i)
			for(int j=0;j<this.rows_nr;++j)
				this.matrix[j][i]*=vector.get(j);
		return this;
	}

	@Override
	public NaiveMatrix mulColumns(Matrix<Double> vector) {
		NaiveMatrix ret = this.duplicate();
		ret.muliColumns(vector);
		return ret;
	}

	@Override
	public NaiveMatrix putRows(int[] indices, Matrix<Double> rows) {
		for(int i=0;i<indices.length;++i)
			for(int j=0;j<this.cols_nr;++j)
				this.put(indices[i], j,rows.get(i, j));
		return this;
	}

	@Override
	public NaiveMatrix putColumns(int[] indices, Matrix<Double> columns) {
		for(int i=0;i<indices.length;++i)
			for(int j=0;j<this.rows_nr;++j)
				this.put(j, indices[i],columns.get(j, i));
		return this;
	}



}

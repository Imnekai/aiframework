package AIFramework.Computation;

public class ExpFunction extends Function<Double>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6480612343153313225L;
	
	private double base;
	
	public ExpFunction(){
		this.base = Math.E;
	}
	
	public ExpFunction(double base){
		this.base = base;
	}

	@Override
	public Double call(Double x) {
		if(this.base==Math.E)
			return Math.exp(x);
		else
			return Math.pow(base, x);
	}

	@Override
	public Double derivative(Double x) {
		if(this.base==Math.E)
			return Math.exp(x);
		else
			return Math.pow(base, x)*Math.log(base);
	}

	@Override
	public ExpFunction duplicate() {
		return new ExpFunction(this.base);
	}

}

package AIFramework.Computation;

public class ReluFunction extends FunctionDouble{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1841905223553822209L;
	
	private double offset=0d;
	
	public ReluFunction(){
		
	}
	
	public ReluFunction(double offset){
		this.offset=offset;
	}
	
	public double getOffset(){
		return this.offset;
	}

	@Override
	public Double call(Double x) {
		return Math.max(0d, x);
	}

	@Override
	public Double derivative(Double x) {
		if(x>0)return 1d;
		return 0d;
	}

	public ReluFunction duplicate(){
		return new ReluFunction();
	}
	

}

package AIFramework.Computation;

import AIFramework.Core.Config;

public abstract class FunctionDouble extends Function<Double>{
	
	public double aproxDeriv(double x,double eps){
		return (call(x+eps)-call(x-eps))/(2*eps);
	}
	
	public boolean testDerivative(double x,double eps){
		double d = aproxDeriv(x,eps);
		return Math.abs(d-this.derivative(x))<Config.EPSILON;
	}
	
	public boolean testDerivative(double x){
		return testDerivative(x,Config.EPSILON);
	}
	
	@Override
	public abstract FunctionDouble duplicate();

}

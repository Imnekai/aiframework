package AIFramework.Computation;

public class SigmFunction extends FunctionDouble{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4348321532563397050L;
	private double l=1d,k=1d,m=0d;
	private double flat_spot = 0d;
	
	public SigmFunction(){
		
	}
	
	/**
	 * function:  L/(1+exp(-k(x-m))
	 */
	public SigmFunction(double l,double k,double m){
		this.l=l;
		this.k=k;
		this.m=m;
	}

	@Override
	public Double call(Double x) {
		return l/(1+Math.exp(-k*(x-m)));
	}

	@Override
	public Double derivative(Double x) {
		double c = call(x);
		return c*(1-c)+this.flat_spot;
	}
	
	public SigmFunction duplicate(){
		SigmFunction ret = new SigmFunction();
		ret.k=this.k;
		ret.l=this.l;
		ret.m = this.m;
		return ret;
	}

	public SigmFunction fixFlatSpot(boolean b) {
		if(b==true)
			this.flat_spot=0.1d;
		else
			this.flat_spot=0d;
		return this;
	}

}

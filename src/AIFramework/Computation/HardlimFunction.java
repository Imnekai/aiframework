package AIFramework.Computation;

public class HardlimFunction extends Function<Double>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4147172776726449329L;
	
	private double offset=0d;
	
	public HardlimFunction(){}
	
	public HardlimFunction(double offset){
		this.offset=offset;
	}

	@Override
	public Double call(Double x) {
		if(x<=offset)
			return 0d;
		else
			return 1d;
	}

	@Override
	public Double derivative(Double x) {
		return 0d;
	}

	@Override
	public Function<Double> duplicate() {
		HardlimFunction ret = new HardlimFunction(this.offset);
		return ret;
	}

}

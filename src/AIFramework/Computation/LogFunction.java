package AIFramework.Computation;

import AIFramework.Core.Utility;

public class LogFunction extends Function<Double>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8741310834550373513L;
	
	private double base;
	
	public LogFunction(){
		this.base = Math.E;
	}
	
	public LogFunction(double base){
		this.base=base;
	}

	@Override
	public Double call(Double x) {
		if(Utility.areEqual(x, 0))
			return 0d;
		if(this.base==Math.E)
			return Math.log(x);
		else
			return Math.log(x)/Math.log(base);
	}

	@Override
	public Double derivative(Double x) {
		if(this.base==Math.E)
			return 1d/x;
		else
			return 1d/(x+Math.log(base));
	}

	@Override
	public Function<Double> duplicate() {
		return new LogFunction(this.base);
	}

}

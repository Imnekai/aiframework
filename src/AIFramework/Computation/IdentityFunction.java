package AIFramework.Computation;

public class IdentityFunction extends FunctionDouble{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4967976792188907345L;

	@Override
	public Double call(Double x) {
		return x;
	}

	@Override
	public Double derivative(Double x) {
		return 1d;
	}
	
	public IdentityFunction duplicate(){
		return new IdentityFunction();
	}

}

package AIFramework.Computation;
import java.util.ArrayList;
import java.util.Arrays;

import org.jblas.DoubleMatrix;
import org.jblas.Singular;
import org.jblas.Solve;

import AIFramework.Core.Config;
import AIFramework.Data.DataPoint;

public class JblasMatrix extends Matrix<Double>{

	private static final long serialVersionUID = -2035787471504829687L;
	private DoubleMatrix matrix;
	
	public JblasMatrix(){
		matrix=new DoubleMatrix();
	}
	
	public <T extends DataPoint> JblasMatrix(ArrayList<T> data_points){
		this.set(data_points);
	}
	
	public JblasMatrix(int rows,int cols){
		set(rows,cols);
	}
	
	public JblasMatrix(DoubleMatrix matrix){
		this.matrix = matrix;
	}

	@Override
	public JblasMatrix mul(Matrix<Double> b){
		JblasMatrix b2 = (JblasMatrix)b;
		if(this.matrix.columns!=b2.matrix.rows)throw new ArithmeticException("Wrong matrix shape");
		JblasMatrix ret=new JblasMatrix(this.matrix.rows,b2.matrix.columns);
		ret.matrix = this.matrix.mmul(b2.matrix);
		return ret;
	}

	@Override
	public JblasMatrix mul(Double a) {
		JblasMatrix ret = this.duplicate();
		ret.muli(a);
		return ret;
	}
	
	
	/**
	 * Matrix multiplication in-place.
	 * @param Other matrix
	 * @return Reference to this
	 */
	@Override
	public JblasMatrix muli(Matrix<Double> b){
		JblasMatrix b2 = (JblasMatrix)b;
		if(this.matrix.columns!=b2.matrix.rows)throw new ArithmeticException("Wrong matrix shape");
		if(b2.matrix.columns!=this.matrix.columns)
			this.matrix = this.matrix.mmul(b2.matrix);
		else
			this.matrix.mmuli(b2.matrix);
		return this;
	}

	@Override
	public JblasMatrix muli(Double a) {
		this.matrix.muli(a);
		return this;
	}

	@Override
	public JblasMatrix add(Matrix<Double> b){
		JblasMatrix ret = this.duplicate();
		ret.addi(b);
		return ret;
	}
	
	@Override
	public JblasMatrix addi(Matrix<Double> b){
		JblasMatrix b2 = (JblasMatrix)b;
		if(b2.rowNr()!=matrix.rows||b2.colNr()!=matrix.columns)throw new ArithmeticException("Matrices have different shape");
		this.matrix.addi(b2.matrix);
		return this;
	}
	
	@Override
	public JblasMatrix sub(Matrix<Double> b){
		JblasMatrix ret = this.duplicate();
		ret.subi(b);
		return ret;
	}
	
	@Override
	public JblasMatrix subi(Matrix<Double> b){
		JblasMatrix b2 = (JblasMatrix)b;
		if(b2.rowNr()!=matrix.rows||b2.colNr()!=matrix.columns)throw new ArithmeticException("Matrices have different shape");
		this.matrix.subi(b2.matrix);
		return this;
	}

	@Override
	public JblasMatrix transpose() {
		JblasMatrix ret=new JblasMatrix();
		ret.matrix = this.matrix.transpose();
		return ret;
	}

	@Override
	public JblasMatrix inverse() {
		JblasMatrix ret=new JblasMatrix();
		ret.matrix = Solve.pinv(this.matrix);
		return ret;
	}
	

	@Override
	public int colNr() {
		return this.matrix.columns;
	}

	@Override
	public int rowNr() {
		return this.matrix.rows;
	}


	@Override
	public Double get(int row, int col) throws IndexOutOfBoundsException {
		return matrix.get(row,col);
	}

	@Override
	public void put(int row, int col, Double val) throws IndexOutOfBoundsException {
		this.matrix.put(row, col,val);
	}

	
	public String toString(){
		return matrix.toString();
	}


	@Override
	public void set(int rows, int columns) {
		matrix= DoubleMatrix.zeros(rows,columns);
		
	}

	@Override
	public JblasMatrix identity(int n) {
		JblasMatrix ret=new JblasMatrix(n,n);
		ret.matrix = DoubleMatrix.eye(n);
		return ret;
	}

	@Override
	public boolean isEqual(Matrix<Double> b) throws ArithmeticException{
		if(this.rowNr()!=b.rowNr()||this.colNr()!=b.colNr())throw new ArithmeticException("Matrices of different shape");
		
		/*if(b.getClass()== JblasMatrix.class){
			JblasMatrix b2 = (JblasMatrix) b;
			return b2.matrix.compare(this.matrix, Config.EPSILON);
		}*/
		for(int i=0;i<this.size();++i)
				if(Double.isNaN(this.get(i))||Double.isNaN(b.get(i))||
						Math.abs(this.get(i)-b.get(i))>Config.EPSILON)
							return false;
		return true;
	}

	@Override
	public <C extends DataPoint> Matrix<Double> set(ArrayList<C> data_points) {
		if(data_points==null||data_points.size()==0)throw new UnsupportedOperationException("No data points");
		this.matrix=new DoubleMatrix(data_points.size(),data_points.get(0).getDimension());
		for(int i=0;i<this.matrix.rows;++i)
			for(int j=0;j<this.matrix.columns;++j)
				matrix.put(i,j,data_points.get(i).getFeature(j));
		return this;
	}

	@Override
	public JblasMatrix duplicate() {
		JblasMatrix ret=new JblasMatrix();
		ret.matrix = this.matrix.dup();
		return ret;
	}

	@Override
	public JblasMatrix getColumnMeans() {
		JblasMatrix ret=new JblasMatrix();
		ret.matrix = matrix.columnMeans();
		return ret;
	}

	@Override
	public JblasMatrix getColumnsAbs() {
		JblasMatrix ret=new JblasMatrix(1,this.colNr());
		DoubleMatrix max = this.matrix.columnMaxs();
		DoubleMatrix min = this.matrix.columnMins();
		for(int i=0;i<this.colNr();++i)
				ret.put(i, Math.max(Math.abs(max.get(i)), Math.abs(min.get(i))));
		return ret;
	}
	
	@Override
	public JblasMatrix getRowsAbs() {
		JblasMatrix ret=new JblasMatrix(1,this.rowNr());
		DoubleMatrix max = this.matrix.rowMaxs();
		DoubleMatrix min = this.matrix.rowMins();
		for(int i=0;i<this.rowNr();++i)
				ret.put(i, Math.max(Math.abs(max.get(i)), Math.abs(min.get(i))));
		return ret;
	}

	@Override
	public JblasMatrix diviRows(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.diviRowVector(vector2.matrix);
		return this;
	}
	
	@Override
	public JblasMatrix divRows(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.diviRows(vector);
		return ret;
	}
	
	@Override
	public JblasMatrix diviColumns(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.diviColumnVector(vector2.matrix);
		return this;
	}
	
	@Override
	public JblasMatrix divColumns(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.diviColumns(vector);
		return ret;
	}
	
	@Override
	public JblasMatrix muliRows(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.muliRowVector(vector2.matrix);
		return this;
	}
	
	@Override
	public JblasMatrix mulRows(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.muliRows(vector);
		return ret;
	}
	
	@Override
	public JblasMatrix muliColumns(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.muliColumnVector(vector2.matrix);
		return this;
	}
	
	@Override
	public JblasMatrix mulColumns(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.muliColumns(vector);
		return ret;
	}
	
	@Override
	public JblasMatrix subRows(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.subiRows(vector);
		return ret;
	}

	@Override
	public JblasMatrix subiRows(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.subiRowVector(vector2.matrix);
		return this;
	}
	
	@Override
	public JblasMatrix subColumns(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.subiColumns(vector);
		return ret;
	}
	
	@Override
	public JblasMatrix subiColumns(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.subiColumnVector(vector2.matrix);
		return this;
	}
	
	@Override
	public Matrix<Double> addiRows(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.addiRowVector(vector2.matrix);
		return this;
	}
	
	@Override
	public Matrix<Double> addRows(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.addiRows(vector);
		return this;
	}
	
	@Override
	public Matrix<Double> addiColumns(Matrix<Double> vector) {
		if(!vector.isVector())throw new IllegalArgumentException();
		JblasMatrix vector2 = (JblasMatrix)vector;
		this.matrix.addiColumnVector(vector2.matrix);
		return this;
	}
	
	@Override
	public Matrix<Double> addColumns(Matrix<Double> vector) {
		JblasMatrix ret = this.duplicate();
		ret.addiColumns(vector);
		return this;
	}


	@Override
	public JblasMatrix getRowMaxs() {
		JblasMatrix ret=new JblasMatrix();
		ret.matrix = this.matrix.rowMaxs();
		return ret;
	}
	
	@Override
	public JblasMatrix getRowMaxsInd() {
		JblasMatrix ret=new JblasMatrix(1,this.rowNr());
		int[] args = this.matrix.rowArgmaxs();
		for(int i=0;i<this.rowNr();++i){
			ret.put(i, (double)args[i]);
		}
		return ret;
	}

	@Override
	public Matrix<Double> concatH(Matrix<Double> b) {
		JblasMatrix b2 = (JblasMatrix) b;
		if(b2.colNr()!=this.colNr())throw new IllegalArgumentException("Matrices must have the same number of columns.");
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = DoubleMatrix.concatHorizontally(this.matrix, b2.matrix);
		return ret;
	}

	@Override
	public Matrix<Double> concatV(Matrix<Double> b) {
		JblasMatrix b2 = (JblasMatrix) b;
		if(b2.rowNr()!=this.rowNr())throw new IllegalArgumentException("Matrices must have the same number of rows.");
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = DoubleMatrix.concatVertically(this.matrix, b2.matrix);
		return ret;
	}

	@Override
	public Matrix<Double> emuli(Matrix<Double> b) {
		if(b.rowNr()!=matrix.rows||b.colNr()!=matrix.columns)throw new ArithmeticException("Matrices have different shape");
		JblasMatrix b2 = (JblasMatrix)b;
		this.matrix.muli(b2.matrix);
		return this;
	}
	
	@Override
	public Matrix<Double> emul(Matrix<Double> b) {
		return this.duplicate().emuli(b);
	}
	
	@Override
	public Matrix<Double> fill(Double val){
		this.matrix.fill(val);
		return this;
	}

	@Override
	public Double getSum() {
		return this.matrix.sum();
	}

	@Override
	public JblasMatrix getRowSum() {
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = this.matrix.rowSums();
		return ret;
	}

	@Override
	public JblasMatrix getColumnSum() {
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = this.matrix.columnSums();
		return ret;
	}

	@Override
	public JblasMatrix getRows(int[] indices) {
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = this.matrix.getRows(indices);
		return ret;
	}

	@Override
	public JblasMatrix getColumns(int[] indices) {
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = this.matrix.getColumns(indices);
		return ret;
	}
	
	@Override
	public JblasMatrix putRows(int[] indices, Matrix<Double> rows) {
		JblasMatrix other = (JblasMatrix)rows;
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = this.matrix.dup();
		for(int i=0;i<indices.length;++i)
			ret.matrix.putRow(indices[i], other.matrix.getRow(i));
		return ret;
	}

	@Override
	public JblasMatrix putColumns(int[] indices, Matrix<Double> columns) {
		JblasMatrix other = (JblasMatrix)columns;
		JblasMatrix ret = new JblasMatrix();
		ret.matrix = this.matrix.dup();
		for(int i=0;i<indices.length;++i)
			ret.matrix.putColumn(indices[i], other.matrix.getRow(i));
		return ret;
	}

	/**
	 * Fill the matrix with uniformly distributed values in 0..1
	 * @return Reference to current matrix.
	 */
	@Override
	public JblasMatrix rand() {
		this.matrix = DoubleMatrix.rand(this.rowNr(), this.colNr());
		return this;
	}

	@Override
	public JblasMatrix[] getSVD() {
		DoubleMatrix[] svd = Singular.fullSVD(matrix);
		JblasMatrix[] ret = new JblasMatrix[3];
		for(int i=0;i<3;++i)
			ret[i] = new JblasMatrix(svd[i]);
		return ret;
	}

	

	
	

}

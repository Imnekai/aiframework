package AIFramework.Computation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;


import AIFramework.Core.Config;
import AIFramework.Data.DataPoint;

public abstract class Matrix<T> implements Serializable{
	
	public abstract Matrix<T> duplicate();
	public abstract Matrix<T> mul(Matrix<T> b);
	public abstract Matrix<T> mul(T a);
	public abstract Matrix<T> muli(Matrix<T> b);
	public abstract Matrix<T> muli(T a);
	
	/**
	 * Element-wise multiplication(in-place)
	 */
	public abstract Matrix<T> emuli(Matrix<T> b);
	public abstract Matrix<T> emul(Matrix<T> b);
	/**
	 * Divide the matrix rows to the specified row vector(in place)
	 * @param vector
	 * @return Reference to the same matrix
	 */
	public abstract Matrix<T> diviRows(Matrix<T> vector);
	/**
	 * Subtract the specified row vector from all the matrix rows(in place)
	 * @param vector
	 * @return Reference to the same matrix
	 */
	public abstract Matrix<T> divRows(Matrix<T> vector);
	public abstract Matrix<T> diviColumns(Matrix<T> vector);
	public abstract Matrix<T> divColumns(Matrix<T> vector);
	public abstract Matrix<T> mulRows(Matrix<T> vector);
	public abstract Matrix<T> muliRows(Matrix<T> vector);
	public abstract Matrix<T> muliColumns(Matrix<T> vector);
	public abstract Matrix<T> mulColumns(Matrix<T> vector);
	public abstract Matrix<T> subiRows(Matrix<T> vector);
	public abstract Matrix<T> subRows(Matrix<T> vector);
	public abstract Matrix<T> subiColumns(Matrix<T> vector);
	public abstract Matrix<T> subColumns(Matrix<T> vector);
	public abstract Matrix<T> addiRows(Matrix<T> vector);
	public abstract Matrix<T> addRows(Matrix<T> vector);
	public abstract Matrix<T> addiColumns(Matrix<T> vector);
	public abstract Matrix<T> addColumns(Matrix<T> vector);
	public abstract Matrix<T> add(Matrix<T> b);
	public abstract Matrix<T> addi(Matrix<T> b);
	public abstract Matrix<T> sub(Matrix<T> b);
	public abstract Matrix<T> subi(Matrix<T> b);
	public abstract Matrix<T> transpose();
	public abstract Matrix<T> inverse();
	public abstract Matrix<T> getColumnMeans();
	public abstract Matrix<T>[] getSVD();
	public abstract Matrix<T> rand();
	/**
	 * 
	 * @return Absolute value for each column
	 */
	public abstract Matrix<T> getColumnsAbs();
	public abstract Matrix<T> getRowsAbs();
	public abstract Matrix<T> getRowMaxs();
	/**
	 * 
	 * @return Indices of rows maximum elements
	 */
	public abstract Matrix<T> getRowMaxsInd();
	public abstract Matrix<T> concatH(Matrix<T> b);
	public abstract Matrix<T> concatV(Matrix<T> b);
	
	public abstract void set(int rows,int columns);
	public abstract <C extends DataPoint> Matrix<T> set(ArrayList<C> data_points);

	public abstract T getSum();
	public abstract Matrix<T> getRowSum();
	public abstract Matrix<T> getColumnSum();
	
	public abstract int colNr();
	public abstract int rowNr();
	
	public Matrix<T> getRow(int row){
		return this.getRows(new int[]{row});
	}
	public Matrix<T> getColumn(int column){
		return this.getColumns(new int[]{column});
	}
	public Matrix<T> putRow(int row,Matrix<T> rows){
		return this.putRows(new int[]{row},rows);
	}
	public Matrix<T> putColumn(int column,Matrix<T> columns){
		return this.putColumns(new int[]{column},columns);
	}
	
	public abstract Matrix<T> getRows(int[] indices);
	public abstract Matrix<T> getColumns(int[] indices);
	public abstract Matrix<T> putRows(int[] indices,Matrix<T> rows);
	public abstract Matrix<T> putColumns(int[] indices,Matrix<T> columns);
	
	public ArrayList<T> toArrayList(){
		if(!this.isVector())throw new UnsupportedOperationException("Matrix is not a vector");
		ArrayList<T> ret=new ArrayList<T>();
		for(int i=0;i<this.size();++i)
			ret.add(this.get(i));
		return ret;
	}
	
	public boolean isVector(){
		return (this.colNr()==1||this.rowNr()==1);
	}
	
	public int size(){
		return this.colNr()*this.rowNr();
	}
	
	public T get(int ind){
		return this.get(ind/this.colNr(), ind%this.colNr());
	}
	
	public void put(int ind,T val){
		this.put(ind/this.colNr(), ind%this.colNr(), val);
	}
	
	public abstract T get(int row,int col) throws IndexOutOfBoundsException;
	public abstract void put(int row,int col,T val) throws IndexOutOfBoundsException;
	
	public Matrix<T> fill(T val){
		for(int i=0;i<this.rowNr();++i)
			for(int j=0;j<this.colNr();++j)
				this.put(i, j, val);
		return this;
	}
	
	public abstract boolean isEqual(Matrix<T> b) throws ArithmeticException;
	
	public abstract Matrix identity(int n);
	
	public static Matrix createIdentity(int n){
		if(n<=0)throw new IllegalArgumentException("The dimension must be a strictly pozitive integer");
		Matrix ret = Matrix.newInstance();
		return ret.identity(n);
	}
	
	/**
	 * Apply a function to all elements
	 * @param Function f
	 * @return Reference to the new matrix
	 */
	public Matrix<T> apply(Function<T> f){
		return this.duplicate().applyi(f);
	}
	
	/**
	 * Apply a function to all elements(in-place)
	 * @param Function f
	 * @return Reference to this
	 */
	public Matrix<T> applyi(Function<T> f){
		for(int i=0;i<this.rowNr();++i)
			for(int j=0;j<this.colNr();++j)
				this.put(i, j, f.call(this.get(i, j)));
		return this;
	}
	
	/**
	 * Apply the derivative of a function to all elements
	 * @param Function f
	 * @return Reference to the new matrix
	 */
	public Matrix<T> apply_derivative(Function<T> f){
		return this.duplicate().applyi_derivative(f);
	}
	
	/**
	 * Apply the derivative of a function to all elements(in-place)
	 * @param Function f
	 * @return Reference to this
	 */
	public Matrix<T> applyi_derivative(Function<T> f){
		for(int i=0;i<this.rowNr();++i)
			for(int j=0;j<this.colNr();++j)
				this.put(i, j, f.derivative(this.get(i, j)));
		return this;
	}
	
	public boolean match(Matrix<T> b){
		return this.colNr()==b.colNr()&&this.rowNr()==b.rowNr();
	}
	
	public int[] getShape(){
		int[] ret = new int[2];
		ret[0] = this.rowNr();
		ret[1]= this.colNr();
		return ret;
	}
	
	public T getL2Loss(){
		return this.emul(this).getSum();
	}
	
	public double getNorm2(){
		return Math.sqrt((double)this.getL2Loss());
	}
	
	public double dist2(Matrix<T> other){
		return this.sub(other).getNorm2();
	}
	
	public void writeToFile(String file_path){
		  try
		  {
		     FileOutputStream fileOut = new FileOutputStream(file_path);
		     ObjectOutputStream out = new ObjectOutputStream(fileOut);
		     out.writeObject(this);
		     out.close();
		     fileOut.close();
		  }catch(IOException i)
		  {
		      i.printStackTrace();
		  }
	}
	
	public static Matrix readFromFile(String file_path){
		Matrix ret=null;
		  try
	      {
	         FileInputStream fileIn = new FileInputStream(file_path);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         ret = (Matrix) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	      }catch(ClassNotFoundException c)
	      {
	         c.printStackTrace();
	      }
		  return ret;
	}
	
	public static Matrix newInstance(){
		Matrix ret = null;
		try {
			ret = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	
	public static Matrix newInstance(int rows,int cols){
		Matrix ret = Matrix.newInstance();
		ret.set(rows,cols);
		return ret;
	}
}

package AIFramework.Computation;

import java.io.Serializable;

public abstract class Function<T> implements Serializable{
	
	public abstract T call(T x);
	public abstract T derivative(T x);
	
	public Matrix<T> call(Matrix<T> m){
		return m.apply(this);
	}
	
	public Matrix<T> derivative(Matrix<T> m){
		return m.apply_derivative(this);
	}

	public abstract Function<T> duplicate();
	
}

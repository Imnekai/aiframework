package AIFramework.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Core.Utility;

public class UtilityTest {
	
	ArrayList<Double> list;

	@Before
	public void setUp() throws Exception {
		list = new ArrayList<Double>();
		Double[] elems = new Double[]{-2d,3d,100d,23d};
		for(int i=0;i<elems.length;++i)
			list.add(elems[i]);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMaxInd() {
		assertEquals(Utility.indexOfMax(list),2);
	}
	
	private boolean unique(int[] a){
		for(int i=1;i<a.length;++i)
			for(int j=0;j<i;++j)
				if(a[i]==a[j])return false;
		return true;
	}
	
	@Test
	public void testRandInts(){
		int min=-2,max=10;
		int nr = 6;
		int[] ind = Utility.randInts(min, max, nr);
		assertTrue(ind.length==nr);
		assertTrue(unique(ind));
		for(int i=0;i<nr;++i)
			assertTrue(ind[i]>=min&&ind[i]<=max);
		
		nr = max-min+1;
		ind = Utility.randInts(min, max, nr);
		assertTrue(unique(ind));
		assertTrue(ind.length==nr);
		for(int i=0;i<nr;++i)
			assertTrue(ind[i]>=min&&ind[i]<=max);
	}

}

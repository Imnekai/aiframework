package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.NNlayers.DropoutLayer;

public class DropoutLayerTest {
	
	private DropoutLayer dropout_layer;

	@Before
	public void setUp() throws Exception {
		dropout_layer = new DropoutLayer();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Matrix<Double> mat = Matrix.newInstance(50,50);
		mat.fill(Math.PI);
		double nr=0;
		double margin=0.05d;
		Matrix<Double> output = dropout_layer.forward(mat, true);
		for(int i=0;i<mat.size();++i)
			if(output.get(i)==Math.PI)
				nr+=1;
		nr/=output.size();
		assertTrue(nr>=dropout_layer.getDropProbability()-margin&&nr<=dropout_layer.getDropProbability()+margin);
		
		mat.fill(Math.PI);
		Matrix<Double> back = dropout_layer.backward(mat, true);
		for(int i=0;i<back.size();++i)
			assertEquals(back.get(i),output.get(i),Config.EPSILON);
	}
	
	

}

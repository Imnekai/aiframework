package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Data.DataSet;
import AIFramework.LossFunctions.SquaredLoss;
import AIFramework.NNlayers.DropoutLayer;
import AIFramework.NNlayers.SigmLayer;
import AIFramework.SupervisedModels.FCNN;

public class FCNNtest {
	
	private FCNN net;

	@Before
	public void setUp() throws Exception {
		net = new FCNN(new SquaredLoss(),2);
		//net.setVerbose(true);
		net.setBatchSize(16);
		net.setLearningRate(0.01d);
		//net.setLearningRateDecay(0.99d);
		net.setRegularization(0.1d);
		net.setEpochs(1000);
		net.set(20,20,1);
		net.setMomentum(0.9d);
		//net.addLayer(new DropoutLayer());
		net.addLayer(new SigmLayer());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testXORlearning() {
		double[][] centers = new double[4][2];
		int[] labels=new int[]{0,1,1,0};
		double abs = 5;
		double dist = 0.5d;
		centers[0] = new double[]{-abs,-abs};
		centers[1] = new double[]{-abs,abs};
		centers[2] = new double[]{abs,-abs};
		centers[3] = new double[]{abs,abs};
		DataSet train_set = DataSet.generateBlobs(centers, labels, 10, dist).shuffle();
		DataSet val_set = DataSet.generateBlobs(centers, labels, 4, dist).shuffle();
		DataSet test_set = DataSet.generateBlobs(centers, labels, 4, dist).shuffle();
		
		net.setValidationSet(val_set);
		net.train(train_set);
		
		Matrix<Double> pred = net.predict(test_set);
		assertEquals(pred.size(),test_set.size());
		double acc = 0d;
		Matrix<Double> test_labels = test_set.getLabelMatrix();
		for(int i=0;i<test_set.size();i++)
			if((pred.get(i)<0.5d && test_labels.get(i)==0d)||(pred.get(i)>=0.5d && test_labels.get(i)==1))
				acc+=1d/test_set.size();
			//else System.out.println(test_set.getDataPoints().get(i).getFeatureVector().toString()+" "+test_labels.get(i));
		//System.out.println(test_set.getFeatureMatrix().toString());
		//System.out.println(test_set.getLabelMatrix().toString());
		//System.out.println(acc);
		assertEquals(1d,acc,Config.EPSILON);
		net.writeToFile("fcnn_test");
		
		FCNN net2 = (FCNN)FCNN.readFromFile("fcnn_test");
		pred = net2.predict(test_set);
		assertEquals(pred.size(),test_set.size());
		acc = 0d;
		test_labels = test_set.getLabelMatrix();
		for(int i=0;i<test_set.size();i++)
			if((pred.get(i)<0.5d && test_labels.get(i)==0d)||(pred.get(i)>=0.5d && test_labels.get(i)==1))
				acc+=1d/test_set.size();
		assertEquals(1d,acc,Config.EPSILON);
	}

}

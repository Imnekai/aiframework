package AIFramework.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Data.DataPoint;
import AIFramework.Data.ExtendedIdentityBasisFunction;

public class ExtendedIdentityBFTest {

	ExtendedIdentityBasisFunction eibf;

	@Before
	public void setUp() throws Exception {
		eibf=new ExtendedIdentityBasisFunction(3);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCallException() {
		DataPoint dp=new DataPoint(4);
		eibf.call(dp);
	}
	
	@Test
	public void testCall(){
		DataPoint dp=new DataPoint(3);
		dp.setFeatures(1d,2d,3d);
		assertArrayEquals(new Double[]{1d,2d,3d,1d},eibf.call(dp).toArrayList().toArray());
	}

}

package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Data.DataPoint;
import AIFramework.Data.PolynomialBasisFunction;

public class PolynomialBFTest {
	
	PolynomialBasisFunction pbf;

	@Before
	public void setUp() throws Exception {
		pbf=new PolynomialBasisFunction(3);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCallException(){
		DataPoint dp=new DataPoint(2);
		pbf.call(dp);
	}

	@Test
	public void testCall() {
		DataPoint dp=new DataPoint(1);
		dp.setFeatures(2.0d);
		assertArrayEquals(pbf.call(dp).toArrayList().toArray(),new Double[]{1d,2d,4d,8d});
	}

}

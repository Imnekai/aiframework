package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.LossFunctions.CrossEntropyLoss;
import AIFramework.LossFunctions.SquaredLoss;

public class CrossEntropyLossTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Matrix<Double> input = Matrix.newInstance(2,2);
		Matrix<Double> labels = Matrix.newInstance(2, 2);
		for(int i=0;i<4;++i){
			input.put(i,Math.sin(i*1.23d));
			labels.put(i,(double) (i%2));
		}
		CrossEntropyLoss loss_function = new CrossEntropyLoss();
		loss_function.setRegularizationLoss(0);
		Matrix<Double> grad = loss_function.compute(input, labels).matrix;
		Matrix<Double> grad2 = loss_function.aproxGradient(input, labels);
		assertTrue(grad.isEqual(grad2));
	}

}

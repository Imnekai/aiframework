package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.LossFunctions.SquaredLoss;

public class SquareLossTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Matrix<Double> input = null;
		Matrix<Double> labels = null;
		try {
			input=Config.MatrixClass.newInstance();
			labels=Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		input.set(2,2);
		labels.set(2,2);
		for(int i=0;i<4;++i){
			input.put(i,Math.sin(i*1.23d));
			labels.put(i,Math.cos(i*0.92d));
		}
		SquaredLoss sl = new SquaredLoss();
		sl.setRegularizationLoss(0);
		Matrix<Double> grad = sl.compute(input, labels).matrix;
		Matrix<Double> grad2 = sl.aproxGradient(input, labels);
		assertTrue(grad.isEqual(grad2));
	}

}

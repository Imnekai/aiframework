package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Computation.ReluFunction;
import AIFramework.Core.Config;

public class ReluFunctionTest {

	private ReluFunction relu;
	
	@Before
	public void setUp() throws Exception {
		relu=new ReluFunction();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertEquals(relu.call(3.4d),3.4d,Config.EPSILON);
		assertEquals(relu.call(-4.5d),0d,Config.EPSILON);
		assertEquals(relu.derivative(83d),1d,Config.EPSILON);
		assertEquals(relu.derivative(-5d),0d,Config.EPSILON);
	}
	
	@Test
	public void testMatrix(){
		JblasMatrix mat = new JblasMatrix(2,2);
		mat.put(0, 0,-3d);mat.put(0, 1,6d);mat.put(1, 0,9d);mat.put(1, 1,-32d);
		JblasMatrix new_mat = (JblasMatrix) relu.call(mat);
		JblasMatrix der_mat = (JblasMatrix) relu.derivative(mat);
		for(int i=0;i<new_mat.rowNr();++i)
			for(int j=0;j<new_mat.colNr();++j)
				if(mat.get(i,j)>0){
					assertEquals(new_mat.get(i,j),mat.get(i,j),Config.EPSILON);
					assertEquals(der_mat.get(i, j),1d,Config.EPSILON);
				}
				else{
					assertEquals(new_mat.get(i,j),0d,Config.EPSILON);
					assertEquals(der_mat.get(i, j),0d,Config.EPSILON);
				}
	}

}

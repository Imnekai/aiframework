package AIFramework.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Computation.Matrix;
import AIFramework.Computation.NaiveMatrix;
import AIFramework.Core.Config;

public class JblastMatrixTest {
	
	List<NaiveMatrix> naive_list= new ArrayList<NaiveMatrix>();
	List<JblasMatrix> jblas_list = new ArrayList<JblasMatrix>();

	@Before
	public void setUp() throws Exception {
		Config.setMatrixClass(JblasMatrix.class);
		Double[][] data = new Double[2][2];
		data[0][0]=1d;data[0][1]=2d;data[1][0]=3d;data[1][1]=4d;
		for(int i=0;i<2;++i)
		{
			NaiveMatrix m = new NaiveMatrix(2,2);
			JblasMatrix m2 = new JblasMatrix(2,2);
			for(int x=0;x<2;++x)
				for(int y=0;y<2;++y){
					m.put(x, y, data[x][y]);
					m2.put(x, y, data[x][y]);
				}
			naive_list.add(m);
			jblas_list.add(m2);
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSum() {
		NaiveMatrix c = naive_list.get(0).add(naive_list.get(1));
		JblasMatrix c2 = jblas_list.get(0).add(jblas_list.get(1));
		assertTrue(c.isEqual(c2));
		assertTrue(c2.isEqual(c));
		for(int i=0;i<2;++i)assertTrue(naive_list.get(i).isEqual(jblas_list.get(i)));
	}
	
	@Test
	public void testMul() {
		NaiveMatrix c = naive_list.get(0).mul(naive_list.get(1));
		JblasMatrix c2 = jblas_list.get(0).mul(jblas_list.get(1));
		assertTrue(c.isEqual(c2));
		assertTrue(c2.isEqual(c));
		for(int i=0;i<2;++i)assertTrue(naive_list.get(i).isEqual(jblas_list.get(i)));
	}
	
	@Test
	public void testScalarMul(){
		NaiveMatrix c = naive_list.get(0).mul(3.3d);
		JblasMatrix c2 = jblas_list.get(0).mul(3.3d);
		assertTrue(c.isEqual(c2));
		for(int i=0;i<2;++i)assertTrue(naive_list.get(i).isEqual(jblas_list.get(i)));
	}
	
	@Test
	public void testIdentity(){
		JblasMatrix m = (JblasMatrix) Matrix.createIdentity(4);
		for(int i=0;i<4;++i)
			for(int j=0;j<4;++j)
				if(i==j)assertTrue(m.get(i, j)==1.0d);
				else assertTrue(m.get(i, j)==0.0d);
	}
	
	@Test
	public void testInv(){
		JblasMatrix inv = jblas_list.get(1).inverse();
		assertTrue(inv.mul(jblas_list.get(1)).isEqual(Matrix.createIdentity(2)));
		for(int i=0;i<2;++i)assertTrue(naive_list.get(i).isEqual(jblas_list.get(i)));
	}
	
	@Test
	public void testTranspose(){
		JblasMatrix ok = new JblasMatrix(2,2);
		JblasMatrix trans = jblas_list.get(1).transpose();
		ok.put(0, 0, 1d);ok.put(0, 1, 3d);ok.put(1, 0, 2d);ok.put(1, 1, 4d);
		assertTrue(trans.isEqual(ok));
		for(int i=0;i<2;++i)assertTrue(naive_list.get(i).isEqual(jblas_list.get(i)));
	}
	
	@Test
	public void testToVector(){
		JblasMatrix matrix=new JblasMatrix(3,1);
		for(int i=0;i<3;++i)matrix.put(i, 0, i*1.0d);
		ArrayList<Double> vec = matrix.toArrayList();
		for(int i=0;i<vec.size();++i)assertTrue(vec.get(i)==i*1.0d);
		
		matrix=new JblasMatrix(1,3);
		for(int i=0;i<3;++i)matrix.put(0, i, i*1.0d);
		vec = matrix.toArrayList();
		for(int i=0;i<vec.size();++i)assertTrue(vec.get(i)==i*1.0d);
	}
	
	@Test
	public void testSerialization(){
		jblas_list.get(0).writeToFile("test_Jblas");
		JblasMatrix matrix = (JblasMatrix) Matrix.readFromFile("test_Jblas");
		assertTrue(matrix.isEqual(jblas_list.get(0)));
	}
	
	@Test
	public void testRowMaxsInd(){
		JblasMatrix inds = jblas_list.get(0).getRowMaxsInd();
		for(int i=0;i<2;++i)
			assertEquals(inds.get(i),1d,Config.EPSILON);
	}
	
	@Test
	public void testColumnsAbs(){
		JblasMatrix mat = new JblasMatrix(10,3);
		for(int i=0;i<mat.size();++i)
			mat.put(i,i*Math.pow(-1, i));
		JblasMatrix abs = mat.getColumnsAbs();
		for(int i=0;i<abs.size();++i){
			for(int j=0;j<mat.rowNr();++j)
				assertTrue(abs.get(i)>=Math.abs(mat.get(j,i)));
		}
	}
	
	@Test
	public void testRowsAbs(){
		JblasMatrix mat = new JblasMatrix(10,3);
		for(int i=0;i<mat.size();++i)
			mat.put(i,i*Math.pow(-1, i));
		JblasMatrix abs = mat.getRowsAbs();
		for(int i=0;i<abs.size();++i){
			for(int j=0;j<mat.colNr();++j)
				assertTrue(abs.get(i)>=Math.abs(mat.get(i,j)));
		}
	}
	
	@Test
	public void testSVD(){
		int rows =  7;
		int cols = 5;
		JblasMatrix mat = new JblasMatrix(rows,cols);
		mat.rand();
		JblasMatrix[] svd = mat.getSVD();
		assertEquals(svd.length,3);
		for(int i=1;i<svd[1].size();++i)
			assertTrue(svd[1].get(i)<=svd[1].get(i-1));
		JblasMatrix diag = new JblasMatrix(rows,cols);
		for(int i=0;i<svd[1].size();++i)
			diag.put(i, i,svd[1].get(i));
		JblasMatrix mat2 = svd[0].mul(diag).mul(svd[2].transpose());
		assertTrue(mat.isEqual(mat2));
	}
	
	@Test
	public void testNorm2(){
		JblasMatrix vec = new JblasMatrix(1,2);
		vec.put(0, 3d);vec.put(1, 4d);
		assertEquals(vec.getNorm2(),5d,Config.EPSILON);
	}
	
	@Test
	public void testDist2(){
		JblasMatrix vec1 = new JblasMatrix(1,2);
		JblasMatrix vec2 = new JblasMatrix(1,2);
		vec1.put(0, 1d);vec1.put(1, -2d);
		vec2.put(0, -4d);vec2.put(1, 6d);
		assertEquals(vec1.dist2(vec2),9.433981132056603,Config.EPSILON);
	}

}

package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Data.DataPoint;
import AIFramework.Data.IdentityBasisFunction;

public class IdentityBFTest {
	
	IdentityBasisFunction ibf;

	@Before
	public void setUp() throws Exception {
		ibf=new IdentityBasisFunction(3);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCallException() {
		DataPoint dp=new DataPoint(4);
		ibf.call(dp);
	}
	
	@Test
	public void testCall(){
		DataPoint dp=new DataPoint(3);
		dp.setFeatures(1d,2d,3d);
		assertArrayEquals(dp.getFeatures(),ibf.call(dp).toArrayList().toArray());
	}

}

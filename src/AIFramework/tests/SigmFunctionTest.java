package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.SigmFunction;
import AIFramework.Core.Config;

public class SigmFunctionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		SigmFunction sigm=new SigmFunction();
		assertTrue(sigm.testDerivative(0.6d));
	}

}

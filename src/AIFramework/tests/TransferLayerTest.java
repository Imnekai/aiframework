package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.NNlayers.ReluLayer;
import AIFramework.NNlayers.SigmLayer;

public class TransferLayerTest {
	
	private Matrix<Double> input=null,ones=null;

	@Before
	public void setUp() throws Exception {
		input = Config.MatrixClass.newInstance();
		input.set(4,4);
		for(int i=0;i<input.size();++i)
			input.put(i, (i+0.231f)*Math.pow(-1,i)*Math.PI*0.01d);
		ones = Config.MatrixClass.newInstance();
		ones.set(4,4);
		ones.fill(1d);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRelu() {
		ReluLayer layer = new ReluLayer();
		
		Matrix<Double> grad1 = layer.aproxGrad(input);
		layer.forward(input, true);
		Matrix<Double> grad2 = layer.backward(ones, true);
		
		assertTrue(grad1.isEqual(grad2));
	}
	
	@Test
	public void testSigm() {
		SigmLayer layer = new SigmLayer();
		Matrix<Double> grad1 = layer.aproxGrad(input);
		layer.forward(input, true);
		Matrix<Double> grad2 = layer.backward(ones, true);
		assertTrue(grad1.isEqual(grad2));
	}

}

package AIFramework.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Core.Config;
import AIFramework.Core.LearningModel;
import AIFramework.Data.DataSet;
import AIFramework.Data.ExtendedIdentityBasisFunction;
import AIFramework.Data.IdentityBasisFunction;
import AIFramework.Data.LabeledDataPoint;
import AIFramework.SupervisedModels.LinearRegressionModel;

public class LinearRegressionModelTest {
	
	DataSet data_set;
	LinearRegressionModel lrm;

	@Before
	public void setUp() throws Exception {
		data_set=new DataSet(1,1);
		LabeledDataPoint dp1 = new LabeledDataPoint(new Double[]{0d},new Double[]{1d});
		LabeledDataPoint dp2 = new LabeledDataPoint(new Double[]{2d},new Double[]{5d});
		data_set.add(dp1);data_set.add(dp2);
		
		lrm=new LinearRegressionModel();
		lrm.setRegularization(0d);
		lrm.setBasisFunction(new ExtendedIdentityBasisFunction(1));
		lrm.train(data_set);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testWeights() {
		ArrayList<Double> weights = lrm.getWeights().toArrayList();
		assertEquals(weights.size(),2);
		Double[] ok = new Double[]{2d,1d};
		for(int i=0;i<2;++i)
			assertEquals(ok[i],weights.get(i),Config.EPSILON);
	}
	
	@Test
	public void testPrediction(){
		LabeledDataPoint dp = new LabeledDataPoint(new Double[]{1d},new Double[]{3d});
		ArrayList<Double> prediction = lrm.predict(dp).toArrayList();
		assertEquals(prediction.size(),1);
		assertEquals(prediction.get(0),dp.getLabel(0),Config.EPSILON);
	}
	
	@Test
	public void testPredictionMatrix(){
		JblasMatrix mat = new JblasMatrix(3,1);
		for(int i=0;i<mat.rowNr();++i)
			mat.put(i,i*2.3d);
		JblasMatrix prediction = (JblasMatrix) lrm.predict(mat);
		assertTrue(prediction.isVector());
		assertTrue(prediction.size()==3);
		for(int i=0;i<prediction.size();++i)
			assertEquals(prediction.get(i),i*4.6d+1,Config.EPSILON);
	}
	
	@Test
	public void testAccuracy(){
		LabeledDataPoint dp1 = new LabeledDataPoint(new Double[]{-1d},new Double[]{-1d});
		LabeledDataPoint dp2 = new LabeledDataPoint(new Double[]{10d},new Double[]{40d});
		DataSet ds=new DataSet(1,1);
		ds.add(dp1);ds.add(dp2);
		Double accuracy = lrm.test(ds);
		assertEquals(accuracy,.5d,Config.EPSILON);
	}
	
	@Test
	public void testSerialization(){
		lrm.writeToFile("test_lrm");
		LinearRegressionModel lrm2 = (LinearRegressionModel)LearningModel.readFromFile("test_lrm");
		lrm.setBasisFunction(new IdentityBasisFunction(2));
		assertTrue(lrm2!=null);
		
		ArrayList<Double> weights = lrm2.getWeights().toArrayList();
		assertEquals(weights.size(),2);
		Double[] ok = new Double[]{2d,1d};
		for(int i=0;i<2;++i)
			assertEquals(ok[i],weights.get(i),Config.EPSILON);
		
		LabeledDataPoint dp = new LabeledDataPoint(new Double[]{1d},new Double[]{3d});
		ArrayList<Double> prediction = lrm2.predict(dp).toArrayList();
		assertEquals(prediction.size(),1);
		assertEquals(prediction.get(0),dp.getLabel(0),Config.EPSILON);
		
	}

}

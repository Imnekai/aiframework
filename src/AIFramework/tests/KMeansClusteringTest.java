package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Data.DataSet;
import AIFramework.UnsupervisedModels.KMeansClustering;

public class KMeansClusteringTest {
	
	KMeansClustering kmeans=null;

	@Before
	public void setUp() throws Exception {
		kmeans = new KMeansClustering(3);
		//kmeans.setVerbose(true);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		double[][] centers = new double[2][2];
		int[] labels=new int[]{0,1};
		double dist = 0.5d;
		centers[0] = new double[]{-3,-3};
		centers[1] = new double[]{3,-3};
		//centers[2] = new double[]{0,3};
		DataSet train_set = DataSet.generateBlobs(centers, labels, 10, dist).shuffle();
		DataSet val_set = DataSet.generateBlobs(centers, labels, 4, dist).shuffle();
		DataSet test_set = DataSet.generateBlobs(centers, labels, 4, dist).shuffle();
		
		kmeans.train(train_set);
		assertEquals(0d,kmeans.test(val_set),Config.EPSILON);
		
	}

}

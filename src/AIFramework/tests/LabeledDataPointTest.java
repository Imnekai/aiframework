package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Data.LabeledDataPoint;

public class LabeledDataPointTest {
	
	private LabeledDataPoint data_point;

	@Before
	public void setUp() throws Exception {
		data_point=new LabeledDataPoint(3,2);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLabelSize() {
		assertEquals(2,data_point.getLabelSize());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSetLabelException(){
		data_point.setLabels(0.5d,1d,7.8d);
	}
	
	@Test
	public void testSetLabel(){
		Double[] data=new Double[]{0.4d,5.1d};
		data_point.setLabels(data);
		assertArrayEquals(data_point.getLabels(),data);
	}

}

package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Data.BasisKernelFunction;
import AIFramework.Data.DataPoint;
import AIFramework.Data.DataSet;
import AIFramework.Data.RBFKernel;

public class BasisKernelFunctionTest {
	
	BasisKernelFunction bf;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		DataSet ds = new DataSet(2);
		RBFKernel rbf=new RBFKernel(1d);
		DataPoint dp1 = new DataPoint(2);dp1.setFeatures(-1d,1d);
		DataPoint dp2=new DataPoint(2);dp2.setFeatures(0d,1d);
		ds.add(dp1);ds.add(dp2);
		try {
			bf=new BasisKernelFunction(ds,rbf);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Matrix<Double> ret = bf.call(dp1);
		assertEquals(ret.size(),2);
		assertEquals(ret.get(0),1d,Config.EPSILON);
		assertEquals(ret.get(1),Math.exp(-1),Config.EPSILON);
	}

}

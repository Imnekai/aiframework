package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Core.Config;
import AIFramework.Data.DataPoint;
import AIFramework.Data.DataSet;
import AIFramework.Data.LabeledDataPoint;
import AIFramework.Data.RBFKernel;
import AIFramework.SupervisedModels.LinearClassificationModel;

public class LinearClassificationModelTest {
	
	LinearClassificationModel lcm;

	@Before
	public void setUp() throws Exception {
		lcm=new LinearClassificationModel();
		lcm.setKernelFunction(new RBFKernel(1));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testClassification() {
		DataSet ds=new DataSet(2,1);
		double[] dx={-1d,-1d,1d,1d};
		double[] dy={-1d,1d,-1d,1d};
		LabeledDataPoint[] dps = new LabeledDataPoint[4];
		for(int i=0;i<4;++i){
			dps[i] = new LabeledDataPoint(2,1);
			dps[i].setFeatures(dx[i],dy[i]);
			dps[i].setLabels(dx[i]*dy[i]);
			ds.add(dps[i]);
		}
		ds.encode1ofK();
		lcm.train(ds);
		assertEquals(0,lcm.predict(dps[0]).get(0),Config.EPSILON);
		assertEquals(1,lcm.predict(dps[1]).get(0),Config.EPSILON);
		assertEquals(1,lcm.predict(dps[2]).get(0),Config.EPSILON);
		assertEquals(0,lcm.predict(dps[3]).get(0),Config.EPSILON);
		DataPoint dp = new DataPoint(2);
		dp.setFeatures(0.8,0.7);
		assertEquals(0,lcm.predict(dp).get(0),Config.EPSILON);
		DataPoint dp2 = new DataPoint(2);
		dp2.setFeatures(-0.1,0.1);
		assertEquals(1,lcm.predict(dp2).get(0),Config.EPSILON);
	}

}

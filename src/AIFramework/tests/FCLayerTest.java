package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.IdentityFunction;
import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.NNlayers.FClayer;

public class FCLayerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		FClayer layer = new FClayer(2,3,true);
		layer.setRegularization(0);
		layer.setKeepGradients(true);
		Matrix<Double> input=null,grads=null;
		try {
			input = Config.MatrixClass.newInstance();
			grads = Config.MatrixClass.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		input.set(5, 2);
		grads.set(5,3);
		grads.fill(1d);
		for(int i=0;i<input.size();++i){
			input.put(i,Math.sin(i*1.043d));
		}
		layer.forward(input,true);
		layer.backward(grads, false);
		Matrix<Double> w_grad = layer.getWeightsGrad();
		Matrix<Double> w_grad2 = layer.aproxWgrad(input);
		assertTrue(w_grad.isEqual(w_grad2));
		Matrix<Double> b_grad = layer.getBiasGrad();
		Matrix<Double> b_grad2 = layer.aproxBgrad(input);
		assertTrue(b_grad.isEqual(b_grad2));
	}

}

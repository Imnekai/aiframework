package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.Matrix;
import AIFramework.Data.DataPoint;
import AIFramework.Data.ExtendedIdentityBasisFunction;

public class DataPointTest {

	DataPoint data_point;
	
	@Before
	public void setUp() throws Exception {
		data_point=new DataPoint(5);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDimension() {
		assertTrue(data_point.getDimension()==5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetException(){
		data_point.setFeatures(1.2d,0.1d,4.6d);
	}
	
	@Test
	public void testSet(){
		Double[] data=new Double[]{1d,2d,4.3d,0.003d,1d};
		data_point.setFeatures(data);
		assertArrayEquals(data_point.getFeatures(),data);
	}
	
	@Test
	public void testFeatureVector(){
		Double[] data=new Double[]{1d,2d,4.3d,0.003d,-1d};
		Double[] ok=new Double[]{1d,2d,4.3d,0.003d,-1d,1d};
		data_point.setFeatures(data);
		Matrix<Double> feature_vector = new ExtendedIdentityBasisFunction(5).call(data_point);
		for(int i=0;i<5;++i)
			assertEquals(feature_vector.get(i, 0),ok[i]);
	}

}

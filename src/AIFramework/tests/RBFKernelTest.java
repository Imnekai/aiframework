package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Core.Config;
import AIFramework.Data.DataPoint;
import AIFramework.Data.RBFKernel;

public class RBFKernelTest {

	RBFKernel rbf;
	@Before
	public void setUp() throws Exception {
		rbf=new RBFKernel(1d);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class)
	public void testException() {
		DataPoint[] dps=new DataPoint[2];
		dps[0]=new DataPoint(2);
		dps[1]=new DataPoint(1);
		rbf.call(dps[0], dps[1]);
	}
	
	@Test
	public void testCall(){
		DataPoint[] dps=new DataPoint[3];
		dps[0]=new DataPoint(2);
		dps[0].setFeatures(1d,2d);
		dps[1]=new DataPoint(2);
		dps[1].setFeatures(2d, 2d);
		dps[2]=new DataPoint(2);
		dps[2].setFeatures(1d, 2d);
		assertEquals(rbf.call(dps[0], dps[2]),1d,Config.EPSILON);
		assertEquals(rbf.call(dps[0], dps[1]),Math.exp(-1),Config.EPSILON);
	}

}

package AIFramework.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import AIFramework.Computation.JblasMatrix;
import AIFramework.Computation.Matrix;
import AIFramework.Core.Config;
import AIFramework.Data.DataPoint;
import AIFramework.Data.DataSet;
import AIFramework.Data.ExtendedIdentityBasisFunction;
import AIFramework.Data.LabeledDataPoint;
import AIFramework.Data.Preprocessor;

public class DataSetTest {
	
	private DataSet data_set;

	@Before
	public void setUp() throws Exception {
		data_set=new DataSet(3,2);
		Config.setMatrixClass(JblasMatrix.class);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDimensions(){
		assertEquals(3,data_set.getDimension());
		assertEquals(2,data_set.getLabelSize());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddException() {
		LabeledDataPoint data_point= new LabeledDataPoint(2,1);
		data_set.add(data_point);
	}
	
	@Test
	public void testDesignMatrix(){
		LabeledDataPoint dp1 = new LabeledDataPoint(new Double[]{1d,2d,3d},new Double[]{34d,55d});
		LabeledDataPoint dp2 = new LabeledDataPoint(new Double[]{-1d,-2d,-3d},new Double[]{34d,55d});
		data_set.add(dp1);data_set.add(dp2);
		Double[][] ok = new Double[][]{new Double[]{1d,2d,3d,1d},new Double[]{-1d,-2d,-3d,1d}};
		JblasMatrix design_matrix = (JblasMatrix) new ExtendedIdentityBasisFunction(3).call(new JblasMatrix(data_set.getLabeledData()));
		for(int i=0;i<2;++i)
			for(int j=0;j<4;++j)
				assertEquals(ok[i][j],design_matrix.get(i, j));
	}
	
	@Test
	public void testLabelMatrix(){
		LabeledDataPoint dp1 = new LabeledDataPoint(new Double[]{1d,2d,3d},new Double[]{34d,55d});
		LabeledDataPoint dp2 = new LabeledDataPoint(new Double[]{-1d,-2d,-3d},new Double[]{3d,4d});
		data_set.add(dp1);data_set.add(dp2);
		Double[][] ok = new Double[][]{new Double[]{34d,55d},new Double[]{3d,4d}};
		JblasMatrix label_matrix = (JblasMatrix) data_set.getLabelMatrix();
		for(int i=0;i<2;++i)
			for(int j=0;j<2;++j)
				assertEquals(ok[i][j],label_matrix.get(i, j));
	}
	
	@Test
	public void testNormalization(){
		DataSet ds = new DataSet(1,1);
		DataPoint[] dps = new DataPoint[4];
		dps[0] = new DataPoint(1);dps[0].setFeatures(-5d);
		dps[1] = new DataPoint(1);dps[1].setFeatures(-3d);
		dps[2] = new DataPoint(1);dps[2].setFeatures(1d);
		dps[3] = new DataPoint(1);dps[3].setFeatures(3d);
		Double[] ok = new Double[]{-0.8d,-0.4d,0.4d,0.8d};
		for(int i=0;i<4;++i)
			ds.add(dps[i]);
		Preprocessor pre = new Preprocessor();
		pre.setCenter(true);
		pre.setNormalize(true);
		Matrix<Double> feature_matrix = ds.getFeatureMatrix();
		pre.setParams(feature_matrix);
		Matrix<Double> after = pre.process(feature_matrix);
		for(int i=0;i<4;++i){
			assertEquals(ok[i],after.get(i),Config.EPSILON);
		}
	}
	
	@Test
	public void testEncode1ofK(){
		DataSet ds = new DataSet(1,1);
		for(int i=0;i<10;++i){
			LabeledDataPoint dp = new LabeledDataPoint(1,1);
			dp.setFeature(0, i%3);
			dp.setLabel(0, i%3);
			ds.add(dp);
		}
		ds.encode1ofK();
		assertTrue(ds.getLabelSize()==3);
		for(LabeledDataPoint dp:ds.getLabeledData()){
			assertEquals(dp.getLabel((int)dp.getFeature(0)),1d,Config.EPSILON);
		}
	}
	
	@Test
	public void testGenerateBlobs(){
		double[][] centers = new double[3][2];
		int[] labels = new int[]{1,2,1};
		int per_center = 10;
		double dist = 0.2d;
		for(int i=0;i<3;++i)
			for(int j=0;j<2;++j)
				centers[i][j]=i;
		DataSet ds = DataSet.generateBlobs(centers, labels, per_center, dist);
		assertEquals(ds.size(),centers.length*per_center);
		for(int i=0;i<centers.length;++i)
			for(int k=0;k<per_center;++k){
				assertEquals(labels[i],ds.getLabeledData().get(i*per_center+k).getLabel(0),Config.EPSILON);
				for(int j=0;j<centers[0].length;++j){
					double feature = ds.getLabeledData().get(i*per_center+k).getFeature(j);
					assertTrue(feature>=centers[i][j]-dist);
					assertTrue(feature<=centers[i][j]+dist);
				}
			}
	}

}
